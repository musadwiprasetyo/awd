package com.example.awt;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UbahPelangganActivity extends AppCompatActivity {
    ImageView back;
    Button ubah;
    EditText nama,alamat,nohp;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    TextView title;
    Intent intent;
    Intent dataedit;
    ProgressBar progressBar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelanggan_ubah);
        title =  (TextView) findViewById(R.id.title);
        nama =  (EditText) findViewById(R.id.nama);
        alamat =  (EditText) findViewById(R.id.alamat);
        nohp =  (EditText) findViewById(R.id.nohp);
        ubah =  (Button) findViewById(R.id.ubah);
        back =  (ImageView) findViewById(R.id.back);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        dataedit = getIntent();
        title.setText("Ubah Data "+dataedit.getStringExtra("nama"));
        nama.setText(dataedit.getStringExtra("nama"));
        nohp.setText(dataedit.getStringExtra("nohp"));
        alamat.setText(dataedit.getStringExtra("alamat"));

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UbahData();
            }
        });
    }
    private void UbahData() {
        if (nama.getText().toString().equals(""))
        {
            Toast.makeText(UbahPelangganActivity.this,"Nama tidak boleh kosong",Toast.LENGTH_SHORT).show();
        }else if(alamat.getText().toString().equals("")){
            Toast.makeText(UbahPelangganActivity.this,"Alamat tidak boleh kosong", Toast.LENGTH_SHORT).show();
        }else if(nohp.getText().toString().equals("")){
            Toast.makeText(UbahPelangganActivity.this,"No HP tidak boleh kosong", Toast.LENGTH_SHORT).show();
        }else{
            progressBar.setVisibility(View.VISIBLE);
            mApiService.ubahPelanggan(dataedit.getStringExtra("id"),
                    nama.getText().toString(),
                    nohp.getText().toString(),
                    alamat.getText().toString())
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            if (response.isSuccessful()){
                                try {
                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                    if (jsonRESULTS.getString("code").equals("200")){
                                        String message = jsonRESULTS.getString("message");
                                        onBackPressed();
                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                    } else {
                                        String message = jsonRESULTS.getString("message");
                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                        progressBar.setVisibility(View.INVISIBLE);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.INVISIBLE);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
