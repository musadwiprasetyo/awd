package com.example.awt;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    SharedPrefManager sharedPrefManager;
    private Boolean islogin;
    ApiService mApiService;
    EditText txtusername,txtpassword;
    String username,password;
    Button btnLogin;
//    RadioGroup akses;
    ProgressBar progressBar;
    AlertDialog.Builder dialog;
    View dialogView;
    LayoutInflater inflater;
//    int akses_id=1;
    Intent intent;
    int id_akses =1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedPrefManager = new SharedPrefManager(this);
        islogin = sharedPrefManager.getISLogin();
        mApiService = BaseApi.getAPIService();
        txtusername    = (EditText) findViewById(R.id.txtusername);
        txtpassword    = (EditText) findViewById(R.id.txtpassword);
        btnLogin       = (Button) findViewById(R.id.btnLogin);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
//        akses      = (RadioGroup) findViewById(R.id.akses);
        progressBar.setVisibility(View.INVISIBLE);
        if(islogin == true ){
            if (sharedPrefManager.getLoginAksesId().equals("1")){
                intent = new Intent(LoginActivity.this,
                        MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }else if(sharedPrefManager.getLoginAksesId().equals("2")){
                intent = new Intent(LoginActivity.this,
                        MainGudangActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }else{
                intent = new Intent(LoginActivity.this,
                        MainKasirActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Login();
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    private void Login() {
        username       = txtusername.getText().toString();
        password       = txtpassword.getText().toString();
//        final String namaakses = ((RadioButton) findViewById(akses.getCheckedRadioButtonId()))
//                        .getText().toString();
//        if (namaakses.equals("Pimpinan")){
//            akses_id = 1;
//        }else if(namaakses.equals("Bag. Gudang")){
//           akses_id = 2;
//        }else{
//            akses_id = 3;
//        }

        if (username.equals(""))
        {
            Toast.makeText(LoginActivity.this,"Username tidak boleh kosong",Toast.LENGTH_SHORT).show();
        }else if(password.equals("")){
            Toast.makeText(LoginActivity.this,"Password tidak boleh kosong", Toast.LENGTH_SHORT).show();
        }else{
            progressBar.setVisibility(View.VISIBLE);
            mApiService.getLogin(username, password)
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            if (response.isSuccessful()){
                                try {
                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                    if (jsonRESULTS.getString("code").equals("200")){
                                        String message = jsonRESULTS.getString("message");
                                        JSONArray data = jsonRESULTS.getJSONArray("data");
                                        for (int i = 0; i < data.length(); i++){
                                            JSONObject arrayElementx = data.getJSONObject(i);
                                            String idkaryawan           = arrayElementx.getString("id_karyawan");
                                            String namakaryawan         = arrayElementx.getString("nama");
                                            String usernamekaryawan     = arrayElementx.getString("username");
                                            String no_hp                = arrayElementx.getString("no_hp");
                                            String akses                = arrayElementx.getString("akses");;
                                            id_akses                    = arrayElementx.getInt("akses_id");
                                            sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_ID,idkaryawan);
                                            sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_NAME,namakaryawan);
                                            sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_USERNAME,usernamekaryawan);
                                            sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_NOHP,no_hp);
                                            sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_AKSES,akses);
                                            sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_AKSES_ID,String.valueOf(id_akses));
                                            sharedPrefManager.saveLOGINBoolean(SharedPrefManager.IS_LOGIN, true);
                                        }
                                        if (id_akses==1){
                                            intent = new Intent(LoginActivity.this,
                                                    MainActivity.class);
                                            finish();
                                        }else if(id_akses==2){
                                             intent = new Intent(LoginActivity.this,
                                                    MainGudangActivity.class);
                                             finish();
                                        }else{
                                             intent = new Intent(LoginActivity.this,
                                                     MainKasirActivity.class);
                                             finish();
                                        }
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                    } else {
                                        String message = jsonRESULTS.getString("message");
                                        setErrorLoading(message);
                                        progressBar.setVisibility(View.INVISIBLE);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                    String message = jsonRESULTS.getString("message");
                                    setErrorLoading(message);
                                    progressBar.setVisibility(View.INVISIBLE);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            setErrorLoading("Periksa Koneksi Internet Anda");
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });
        }
    }
    public void setErrorLoading(String message) {
        new AlertDialog.Builder(this).setTitle("UPS").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }

    public void setSuccessLoading(String message) {
        new AlertDialog.Builder(this).setTitle("Yes").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
}
