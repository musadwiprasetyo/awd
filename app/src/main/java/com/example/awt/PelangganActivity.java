package com.example.awt;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.adapter.BagGudangAdapter;
import com.example.awt.adapter.KasirAdapter;
import com.example.awt.adapter.PelangganAdapter;
import com.example.awt.adapter.PimpinanAdapter;
import com.example.awt.adapter.SupplierAdapter;
import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.Karyawan;
import com.example.awt.model.Pelanggan;
import com.example.awt.model.Supplier;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PelangganActivity extends AppCompatActivity implements PelangganAdapter.ClickFunction  {
    ImageView back,tambah;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    TextView nodatatransaksi;
    PelangganAdapter pelangganAdapter;
    private List<Pelanggan> pelangganList;
    ListView listView;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelanggan);
        back =  (ImageView) findViewById(R.id.back);
        tambah =  (ImageView) findViewById(R.id.tambah);
        listView      = (ListView) findViewById(R.id.listView);
        progressBar  = (ProgressBar) findViewById(R.id.progressBar);
        nodatatransaksi  = (TextView) findViewById(R.id.nodatatransaksi);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        progressBar.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TambahPelangganActivity.class);
                startActivity(intent);
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                dataPelanggan();
            }
        },1000);
    }

    private void dataPelanggan() {
        progressBar.setVisibility(View.INVISIBLE);
        mApiService.getPelanggan()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<Pelanggan> pelangganArrayList = new ArrayList<>();
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        pelangganArrayList.add(new Pelanggan(datarray.getInt("id_pelanggan"),
                                                datarray.getString("nama"),
                                                datarray.getString("no_hp"),
                                                datarray.getString("alamat")));
                                    }
                                    nodatatransaksi.setVisibility(View.INVISIBLE);
                                    if (pelangganArrayList.size()==0){
                                        nodatatransaksi.setVisibility(View.VISIBLE);
                                        nodatatransaksi.setText("Data Pelanggan tidak ada.");
                                    }
                                    pelangganAdapter = new PelangganAdapter(PelangganActivity.this, pelangganArrayList,PelangganActivity.this);
                                    listView.setAdapter(pelangganAdapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(PelangganActivity.this, message, Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(PelangganActivity.this, message, Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.INVISIBLE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(PelangganActivity.this, "Periksa Koneksi Internet Anda.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        dataPelanggan();
        super.onResume();
    }
    @Override
    public void klikubah(String id, String nama, String nohp, String alamat) {
        Intent intent = new Intent(getApplicationContext(), UbahPelangganActivity.class);
        intent.putExtra("id",id);
        intent.putExtra("nama",nama);
        intent.putExtra("nohp",nohp);
        intent.putExtra("alamat",alamat);
        startActivity(intent);
    }

    @Override
    public void klikhapus(final String id) {
        new AlertDialog.Builder(PelangganActivity.this).setTitle("Apakah anda yakin ingin menghapus data ini?")
                .setCancelable(true)
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mApiService.hapusPelanggan(id)
                        .enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                if (response.isSuccessful()){
                                    try {
                                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                        if (jsonRESULTS.getString("code").equals("200")){
                                            String message = jsonRESULTS.getString("message");
                                            setSuccessLoading(message);
                                            dataPelanggan();
                                        } else {
                                            String message = jsonRESULTS.getString("message");
                                            setErrorLoading(message);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    setErrorLoading("Gagal menghapus data");
                                }
                            }
                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                setErrorLoading("Periksa Koneksi Internet Anda");
                            }
                        });


            }}).show();
    }
    public void setErrorLoading(String message) {
        new AlertDialog.Builder(PelangganActivity.this).setTitle("UPS").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
    public void setSuccessLoading(String message) {
        new AlertDialog.Builder(PelangganActivity.this).setTitle("Yes").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
}
