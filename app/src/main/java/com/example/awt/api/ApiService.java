package com.example.awt.api;

import com.example.awt.model.Karyawan;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiService {

    @FormUrlEncoded
    @POST("api/login.php")
    Call<ResponseBody> getLogin(@Field("username") String username,
                                @Field("password") String password);

    @GET("api/pimpinan.php")
    Call<ResponseBody> getPimpinan();

    @GET("api/gudang.php")
    Call<ResponseBody> getGudang();

    @GET("api/kasir.php")
    Call<ResponseBody> getKasir();

    @FormUrlEncoded
    @POST("api/tambahkaryawan.php")
    Call<ResponseBody> simpanKaryawan(@Field("nama") String nama,
                                      @Field("username") String username,
                                      @Field("nohp") String nohp,
                                      @Field("password") String password,
                                      @Field("akses_id") String akses_id);

    @FormUrlEncoded
    @POST("api/updatekaryawan.php")
    Call<ResponseBody> ubahKaryawan(  @Field("id_karyawan") String id_karyawan,
                                      @Field("nama") String nama,
                                      @Field("username") String username,
                                      @Field("nohp") String nohp,
                                      @Field("password") String password);


    @FormUrlEncoded
    @POST("api/hapuskaryawan.php")
    Call<ResponseBody> hapusKaryawan(@Field("id_karyawan") String id_karyawan);



    @GET("api/supplier.php")
    Call<ResponseBody> getSupplier();

    @FormUrlEncoded
    @POST("api/tambahsupplier.php")
    Call<ResponseBody> simpanSupplier(@Field("nama") String nama,
                                      @Field("alamat") String alamat,
                                      @Field("nohp") String nohp);

    @FormUrlEncoded
    @POST("api/updatesupplier.php")
    Call<ResponseBody> ubahSupplier(  @Field("id_supplier") String id_supplier,
                                      @Field("nama") String nama,
                                      @Field("alamat") String alamat,
                                      @Field("nohp") String nohp);


    @FormUrlEncoded
    @POST("api/hapussupplier.php")
    Call<ResponseBody> hapusSupplier(@Field("id_supplier") String id_supplier);

    @GET("api/pelanggan.php")
    Call<ResponseBody> getPelanggan();

    @FormUrlEncoded
    @POST("api/tambahpelanggan.php")
    Call<ResponseBody> simpanPelanggan(@Field("nama") String nama,
                                       @Field("nohp") String nohp,
                                       @Field("alamat") String alamat);
    @FormUrlEncoded
    @POST("api/updatepelanggan.php")
    Call<ResponseBody> ubahPelanggan(@Field("id_pelanggan") String id_pelanggan,
                                       @Field("nama") String nama,
                                       @Field("nohp") String nohp,
                                       @Field("alamat") String alamat);
    @FormUrlEncoded
    @POST("api/hapuspelanggan.php")
    Call<ResponseBody> hapusPelanggan(@Field("id_pelanggan") String id_pelanggan);

    @FormUrlEncoded
    @POST("api/barang.php")
    Call<ResponseBody> getBarang(@Field("keyword") String keyword);

    @FormUrlEncoded
    @POST("api/tambahbarang.php")
    Call<ResponseBody> simpanBarang(@Field("kategori_id") String kategori_id,
                                       @Field("kode_kategori") String kode_kategori,
                                       @Field("nama") String nama,
                                       @Field("harga_beli") String harga_beli,
                                    @Field("harga") String harga,
                                    @Field("stok_minimal") String stok_minimal,
                                    @Field("keterangan") String keterangan,
                                    @Field("rak") String rak);
    @FormUrlEncoded
    @POST("api/updatebarang.php")
    Call<ResponseBody> ubahBarang(@Field("id_barang") String id_barang,
                                     @Field("nama") String nama,
                                     @Field("hargabeli") String hargabeli,
                                     @Field("harga") String harga,
                                     @Field("keterangan") String keterangan,
                                     @Field("rak") String rak,
                                     @Field("stok") String stok,
                                     @Field("stokminim") String stokminim);
    @FormUrlEncoded
    @POST("api/hapusbarang.php")
    Call<ResponseBody> hapusBarang(@Field("id_barang") String id_barang);


    @GET("api/kategori.php")
    Call<ResponseBody> getKategori();

    @FormUrlEncoded
    @POST("api/tambahkategori.php")
    Call<ResponseBody> simpanKategori(@Field("kode") String kode,
                                    @Field("nama") String nama);
    @FormUrlEncoded
    @POST("api/updatekategori.php")
    Call<ResponseBody> ubahKategori(@Field("id_kategori") String id_kategori,
                                  @Field("kode") String kode,
                                  @Field("nama") String nama);

    @FormUrlEncoded
    @POST("api/hapuskategori.php")
    Call<ResponseBody> hapusKategori(@Field("id_kategori") String id_kategori);


    @GET("api/pemesanan.php")
    Call<ResponseBody> getPemesanan();

    @FormUrlEncoded
    @POST("api/pemesanandetail.php")
    Call<ResponseBody> pemesananDetail(@Field("pemesanan_id") String pemesanan_id);


    @GET("api/penjualan.php")
    Call<ResponseBody> getPenjualan();

    @FormUrlEncoded
    @POST("api/penjualandetail.php")
    Call<ResponseBody> penjualanDetail(@Field("penjualan_id") String penjualan_id);

    @FormUrlEncoded
    @POST("api/barangmasuk.php")
    Call<ResponseBody> barangMasuk(@Field("tgl_start") String tgl_start,@Field("tgl_end") String tgl_end);

    @FormUrlEncoded
    @POST("api/barangkeluar.php")
    Call<ResponseBody> barangKeluar(@Field("tgl_start") String tgl_start,@Field("tgl_end") String tgl_end);

    @GET("api/masterbarang.php")
    Call<ResponseBody> getMasterBarang();

    @GET("api/masterkategori.php")
    Call<ResponseBody> getMasterKategori();

    @FormUrlEncoded
    @POST("api/kirimpemesanan.php")
    Call<ResponseBody> kirimPemesanan(@Field("kodeorder") String kodeorder,
                                      @Field("supplier_id") String supplier_id,
                                      @Field("karyawan_id") String karyawan_id,
                                      @Field("total") String total,
                                      @Field("pembuat") String pembuat,
                                      @Field("tgl") String tgl);

    @FormUrlEncoded
    @POST("api/kirimpemesanandetail.php")
    Call<ResponseBody> kirimPemesananDetail(@Field("kodeorder") String kodeorder,
                                      @Field("barang_id") String barang_id,
                                      @Field("qty") String qty,
                                      @Field("harga_beli") String harga_beli,
                                      @Field("total") String total);

    @GET("api/masterpelanggan.php")
    Call<ResponseBody> getMasterPelanggan();

    @FormUrlEncoded
    @POST("api/kirimpenjualan.php")
    Call<ResponseBody> kirimPenjualan(@Field("kodeorder") String kodeorder,
                                      @Field("pelanggan_id") String pelanggan_id,
                                      @Field("karyawan_id") String karyawan_id,
                                      @Field("total") String total,
                                      @Field("pembuat") String pembuat,
                                      @Field("tgl") String tgl);

    @FormUrlEncoded
    @POST("api/kirimpenjualandetail.php")
    Call<ResponseBody> kirimPenjualanDetail(@Field("kodeorder") String kodeorder,
                                            @Field("barang_id") String barang_id,
                                            @Field("qty") String qty,
                                            @Field("sub_total") String sub_total,
                                            @Field("total") String total,
                                            @Field("tgl") String tgl);


    @FormUrlEncoded
    @POST("api/wma.php")
    Call<ResponseBody> cekWMA(@Field("bulan") String bulan,
                                    @Field("tahun") String tahun,
                                    @Field("kategori_id") String kategori_id);

    @FormUrlEncoded
    @POST("api/wmagroup.php")
    Call<ResponseBody> cekWMAGroup(@Field("bulan") String bulan,
                                   @Field("tahun") String tahun,
                                   @Field("kategori_id") String kategori_id);

    @FormUrlEncoded
    @POST("api/laporanwma.php")
    Call<ResponseBody> getLaporanWMA(@Field("kategori_id") String kategori_id,@Field("tahun") String tahun);


    @GET("api/lap_karyawan.php")
    Call<ResponseBody> getLapKaryawan();


    @FormUrlEncoded
    @POST("api/lap_barang.php")
    Call<ResponseBody> getLapBarang(@Field("tgl_start") String tgl_start,@Field("tgl_end") String tgl_end);

    @GET("api/lap_pelanggan.php")
    Call<ResponseBody> getLapPelanggan();

    @FormUrlEncoded
    @POST("api/lap_pembelian.php")
    Call<ResponseBody> getLapPembelian(@Field("tgl_start") String tgl_start,@Field("tgl_end") String tgl_end);

    @FormUrlEncoded
    @POST("api/lap_penjualan.php")
    Call<ResponseBody> getLapPenjualan(@Field("tgl_start") String tgl_start,@Field("tgl_end") String tgl_end);


    @GET("api/lap_supplier.php")
    Call<ResponseBody> getLapSupplier();

}
