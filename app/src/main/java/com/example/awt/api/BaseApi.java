package com.example.awt.api;

public class BaseApi {
    public static final String BASE_URL_API = "https://awdmranggen.000webhostapp.com/";
//    public static final String BASE_URL_API = "http://192.168.1.2/awd_server/";
    public static ApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(ApiService.class);
    }
}
