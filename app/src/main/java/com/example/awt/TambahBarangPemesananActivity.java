package com.example.awt;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.PilihBarang;
import com.example.awt.model.PilihSupplier;
import com.example.awt.model.Supplier;
import com.example.awt.utils.DatabaseHandler;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahBarangPemesananActivity extends AppCompatActivity {
    ImageView back, tambah;
    Button simpan;
    Spinner barang;
    EditText tgl;
    EditText hargabeli,qty;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    Intent intent;
    ProgressBar progressBar;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;

    DatabaseHandler dbcenter;
    SQLiteDatabase db;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemesanan_tambah_barang);
        hargabeli =  (EditText) findViewById(R.id.hargabeli);
        qty =  (EditText) findViewById(R.id.qty);
        barang =  (Spinner) findViewById(R.id.barang);
        simpan =  (Button) findViewById(R.id.simpan);
        back =  (ImageView) findViewById(R.id.back);
        progressBar =(ProgressBar) findViewById(R.id.progressBar);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        dbcenter = new DatabaseHandler(this);
        db= dbcenter.getReadableDatabase();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        progressBar.setVisibility(View.INVISIBLE);
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpanData();
            }
        });
        dataBarang();
    }
    private void dataBarang() {
        mApiService.getMasterBarang()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<PilihBarang> pilihBarangs = new ArrayList<>();
                                    pilihBarangs.add(new PilihBarang("0","0","Pilih Barang"));
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        pilihBarangs.add(new PilihBarang(datarray.getString("id_barang"),
                                                datarray.getString("kode_barang"),
                                                datarray.getString("nama_barang")));
                                    }
                                    ArrayAdapter<PilihBarang> adapter = new ArrayAdapter<PilihBarang>(TambahBarangPemesananActivity.this, android.R.layout.simple_spinner_dropdown_item,pilihBarangs);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    barang.setAdapter(adapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(TambahBarangPemesananActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(TambahBarangPemesananActivity.this, message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(TambahBarangPemesananActivity.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void showDateDialog(){
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(TambahBarangPemesananActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tgl.setText(dateFormatter.format(newDate.getTime()));
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
    private void SimpanData() {
        PilihBarang pilihBarang = (PilihBarang) barang.getSelectedItem();
        if (pilihBarang.getId().equals("0")){
            Toast.makeText(TambahBarangPemesananActivity.this, "Silahkan Pilih barang terlebih dahulu", Toast.LENGTH_SHORT).show();
        }else if (hargabeli.getText().toString().equals(""))
        {
            Toast.makeText(TambahBarangPemesananActivity.this,"Harga beli tidak boleh kosong",Toast.LENGTH_SHORT).show();
        }else if(qty.getText().toString().equals("")|qty.getText().toString().equals("0")){
            Toast.makeText(TambahBarangPemesananActivity.this,"Qty tidak boleh kosong / 0", Toast.LENGTH_SHORT).show();
        }else{
            progressBar.setVisibility(View.VISIBLE);
            prosesSimpan(Integer.parseInt(pilihBarang.getId()),pilihBarang.getKode(),pilihBarang.getName(),Integer.parseInt(qty.getText().toString()),Integer.parseInt(hargabeli.getText().toString()));
        }
    }
    public void prosesSimpan(int barangid, String kodebarang, String namabarang, int qtynilai, int hargabelibarang) {
        Cursor cursor = db.rawQuery("SELECT * FROM table_pemesanan_detail WHERE barang_id = '"+barangid+"' " +
                "and kode_barang = '"+kodebarang+"' Limit 1", null);
        if (cursor.moveToFirst()) {
            int totalqty  = Integer.parseInt(cursor.getString(4).toString()) + qtynilai;
            int totalvalue = totalqty * hargabelibarang ;
            db.execSQL("UPDATE table_pemesanan_detail SET qty = '"+totalqty+"',harga_beli='"+hargabelibarang+"',total='"+totalvalue+"'" +
                    "WHERE barang_id = '"+barangid+"' and kode_barang = '"+kodebarang+"' ");
            Log.d("Data", "onCreate: UPDATE QTY" + qtynilai);
            Toast.makeText(TambahBarangPemesananActivity.this, "Berhasil mengupdate nilai qty barang di daftar pesanan", Toast.LENGTH_SHORT).show();
            hargabeli.setText("0");
            qty.setText("0");
        }else {
            int totalvalue = qtynilai * hargabelibarang ;
            db.execSQL("insert into table_pemesanan_detail(barang_id, kode_barang,nama_barang, qty, harga_beli,total) values('" +
                    barangid + "','" +
                    kodebarang + "','" +
                    namabarang + "','" +
                    qtynilai + "','" +
                    hargabelibarang + "','" +
                    totalvalue + "')");
            Log.d("Data", "onCreate: SIMPAN KE KERANJANG");
            hargabeli.setText("0");
            qty.setText("0");
            Toast.makeText(TambahBarangPemesananActivity.this, "Berhasil memasukan barang di daftar pesanan", Toast.LENGTH_SHORT).show();
        }
        progressBar.setVisibility(View.INVISIBLE);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
