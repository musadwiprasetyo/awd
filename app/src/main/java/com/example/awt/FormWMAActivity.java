package com.example.awt;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.INotificationSideChannel;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.adapter.KategoriAdapter;
import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.Bulan;
import com.example.awt.model.Kategori;
import com.example.awt.model.PilihBarang;
import com.example.awt.model.PilihKategori;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormWMAActivity extends AppCompatActivity {
    ImageView back,tambah;
    SharedPrefManager sharedPrefManager;
    Spinner bulan, tahun;
    RadioGroup akses;
    Button mulai;
    ApiService mApiService;
    RelativeLayout datasingle;
    TextView bulantahun,penjualan,wma,error,mad;
    private String[] Year = {"2020","2021","2022"};
    ArrayAdapter<Bulan> adapter;
    ArrayAdapter datatahun;
    Spinner kategori;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formwma);
        back =  (ImageView) findViewById(R.id.back);
        tambah =  (ImageView) findViewById(R.id.tambah);
        bulan =  (Spinner) findViewById(R.id.bulan);
        tahun =  (Spinner) findViewById(R.id.tahun);
        akses =  (RadioGroup) findViewById(R.id.akses);
        mulai =  (Button) findViewById(R.id.mulai);
        datasingle =  (RelativeLayout) findViewById(R.id.datasingle);
        kategori =  (Spinner) findViewById(R.id.kategori);
        bulantahun =  (TextView) findViewById(R.id.bulantahun);
        penjualan  =  (TextView) findViewById(R.id.penjualan);
        wma        =  (TextView) findViewById(R.id.wma);
        error      =  (TextView) findViewById(R.id.error);
        mad        =  (TextView) findViewById(R.id.mad);
        ArrayList<Bulan> pilihBulan = new ArrayList<>();
        pilihBulan.add(new Bulan("01","Januari"));
        pilihBulan.add(new Bulan("02","Februari"));
        pilihBulan.add(new Bulan("03","Maret"));
        pilihBulan.add(new Bulan("04","April"));
        pilihBulan.add(new Bulan("05","Mei"));
        pilihBulan.add(new Bulan("06","Juni"));
        pilihBulan.add(new Bulan("07","Juli"));
        pilihBulan.add(new Bulan("08","Agustus"));
        pilihBulan.add(new Bulan("09","September"));
        pilihBulan.add(new Bulan("10","Oktober"));
        pilihBulan.add(new Bulan("11","November"));
        pilihBulan.add(new Bulan("12","Desember"));

        adapter = new ArrayAdapter<Bulan>(FormWMAActivity.this, android.R.layout.simple_spinner_dropdown_item,pilihBulan);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bulan.setAdapter(adapter);

        datatahun = new ArrayAdapter<>(FormWMAActivity.this,
                android.R.layout.simple_spinner_dropdown_item,Year);
        tahun.setAdapter(datatahun);

        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        dataKategori();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mulai();
            }
        });
    }
    private void dataKategori() {
        mApiService.getMasterKategori()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<PilihKategori> pilihKategoriArrayList = new ArrayList<>();
                                    pilihKategoriArrayList.add(new PilihKategori("0","0","Pilih Kategori"));
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        pilihKategoriArrayList.add(new PilihKategori(datarray.getString("id_kategori"),datarray.getString("kode_kategori"),datarray.getString("nama_kategori")));
                                    }
                                    ArrayAdapter<PilihKategori> adapter = new ArrayAdapter<PilihKategori>(FormWMAActivity.this, android.R.layout.simple_spinner_dropdown_item,pilihKategoriArrayList);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    kategori.setAdapter(adapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(FormWMAActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(FormWMAActivity.this, message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(FormWMAActivity.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void Mulai() {

        final Bulan pilihbulan = (Bulan) bulan.getSelectedItem();
        final String tipe = ((RadioButton) findViewById(akses.getCheckedRadioButtonId()))
                .getText().toString();

        bulantahun.setText(pilihbulan.getName().toString()+'-'+tahun.getSelectedItem().toString());
        penjualan.setText("0");
        wma.setText("-");
        error.setText("-");
        mad.setText("-");
        datasingle.setVisibility(View.INVISIBLE);
        PilihKategori pilihkat = (PilihKategori) kategori.getSelectedItem();
        if (pilihkat.getId().equals("0")){
            Toast.makeText(FormWMAActivity.this, "Silahkan Pilih Kategori terlebih dahulu", Toast.LENGTH_SHORT).show();
        }
        else if (pilihbulan.getKode().toString().equals("01") || pilihbulan.getKode().toString().equals("02") || pilihbulan.getKode().toString().equals("03")){
            Toast.makeText(FormWMAActivity.this, "Perhitungan tidak dapat dilakukan. Minimal 3 bulan sebelumnya di tahun yang sama", Toast.LENGTH_SHORT).show();
        }else{
            if(tipe.equals("Single")) {
                datasingle.setVisibility(View.VISIBLE);
                mApiService.cekWMA(pilihbulan.getKode().toString(), tahun.getSelectedItem().toString(),pilihkat.getId())
                        .enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                Log.i("TAG HASIL", response.code() + "");
                                if (response.isSuccessful()) {
                                    try {
                                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                        if (jsonRESULTS.getString("code").equals("200")) {
                                            String message = jsonRESULTS.getString("message");
                                            JSONArray data = jsonRESULTS.getJSONArray("data");
                                            for (int i = 0; i < data.length(); i++) {
                                                JSONObject datarray = data.getJSONObject(i);
                                                bulantahun.setText(pilihbulan.getName().toString() + '-' + tahun.getSelectedItem().toString());
                                                penjualan.setText(datarray.getString("penjualan"));
                                                wma.setText(datarray.getString("wma"));
                                                error.setText(datarray.getString("error"));
                                                mad.setText(datarray.getString("mad"));
                                            }
                                        } else {
                                            String message = jsonRESULTS.getString("message");
                                            Toast.makeText(FormWMAActivity.this, message, Toast.LENGTH_SHORT).show();
                                            bulantahun.setText(pilihbulan.getName().toString() + '-' + tahun.getSelectedItem().toString());
                                            penjualan.setText("0");
                                            wma.setText("-");
                                            error.setText("-");
                                            mad.setText("-");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    try {
                                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                        String message = jsonRESULTS.getString("message");
                                        Toast.makeText(FormWMAActivity.this, message, Toast.LENGTH_SHORT).show();
//                                    progressBar.setVisibility(View.INVISIBLE);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }


                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.i("TAG", "ini terjadi error");
                                Toast.makeText(FormWMAActivity.this, "Periksa Koneksi Internet Anda.", Toast.LENGTH_SHORT).show();
                            }
                        });
            } else {
                datasingle.setVisibility(View.INVISIBLE);
                mApiService.cekWMAGroup(pilihbulan.getKode().toString(), tahun.getSelectedItem().toString(),pilihkat.getId())
                        .enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                Log.i("TAG HASIL", response.code() + "");
                                if (response.isSuccessful()) {
                                    Toast.makeText(FormWMAActivity.this, "Cek di Laporan WMA", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(FormWMAActivity.this, "Gagal di Laporan WMA", Toast.LENGTH_SHORT).show();
                                }


                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.i("TAG", "ini terjadi error");
                                Toast.makeText(FormWMAActivity.this, "Periksa Koneksi Internet Anda.", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        }
        //Toast.makeText(FormWMAActivity.this,tipe, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
