package com.example.awt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.PenjualanDetail;
import com.example.awt.utils.SharedPrefManager;

import org.w3c.dom.Text;

public class LaporanActivity extends AppCompatActivity {
    Button karyawan,supplier,pelanggan,barang,pembelian,penjualan,lapwma;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    Intent intents;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan);

        karyawan =  (Button) findViewById(R.id.karyawan);
        pembelian =  (Button) findViewById(R.id.pembelian);
        supplier =  (Button) findViewById(R.id.supplier);
        pelanggan =  (Button) findViewById(R.id.pelanggan);
        barang =  (Button) findViewById(R.id.barang);
        lapwma =  (Button) findViewById(R.id.lapwma);

        pembelian =  (Button) findViewById(R.id.pembelian);
        penjualan =  (Button) findViewById(R.id.penjualan);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();


        karyawan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanKaryawanActivity.class);
                startActivity(intent);
            }
        });

        supplier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanSupplierActivity.class);
                startActivity(intent);
            }
        });
        pelanggan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanPelangganActivity.class);
                startActivity(intent);
            }
        });
        barang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanBarangActivity.class);
                startActivity(intent);
            }
        });
        pembelian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanPembelianActivity.class);
                startActivity(intent);
            }
        });
        penjualan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanPenjualanActivity.class);
                startActivity(intent);
            }
        });
        lapwma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanWMAActivity.class);
                startActivity(intent);
            }
        });
    }

}