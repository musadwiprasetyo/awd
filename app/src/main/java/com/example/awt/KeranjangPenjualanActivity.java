package com.example.awt;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.adapter.PemesananDetailAdapter;
import com.example.awt.adapter.PenjualanDetailAdapter;
import com.example.awt.adapter.PenjualanDetailListAdapter;
import com.example.awt.adapter.SupplierAdapter;
import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.PemesananDetail;
import com.example.awt.model.PenjualanDetail;
import com.example.awt.model.PilihBarang;
import com.example.awt.model.PilihPelanggan;
import com.example.awt.model.PilihSupplier;
import com.example.awt.model.Supplier;
import com.example.awt.utils.DatabaseHandler;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;

public class KeranjangPenjualanActivity extends AppCompatActivity implements PenjualanDetailListAdapter.ClickFunction {
    ImageView back, tambah;
    Button simpan,hapus,tambahpelanggan;
    Spinner pelanggan;
    EditText tgl;
    TextView penginput,total;
    TextView kodeorder;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    Intent intent;
    ProgressBar progressBar;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    PenjualanDetailListAdapter penjualanDetailAdapter;
    private List<PenjualanDetail> penjualanDetailList;
    ListView listView;
    DatabaseHandler dbcenter;
    SQLiteDatabase db;
    Cursor cursor,cursorx,cx5;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penjualan_keranjang);
        tgl =  (EditText) findViewById(R.id.tgl);
        penginput =  (TextView) findViewById(R.id.penginput);
        total =  (TextView) findViewById(R.id.total);
        kodeorder =  (TextView) findViewById(R.id.kodeorder);
        pelanggan =  (Spinner) findViewById(R.id.pelanggan);
        simpan =  (Button) findViewById(R.id.simpan);
        hapus =  (Button) findViewById(R.id.hapus);
        back =  (ImageView) findViewById(R.id.back);
        tambah =  (ImageView) findViewById(R.id.tambah);
        tambahpelanggan  = (Button) findViewById(R.id.tambahpelanggan);
        listView      = (ListView) findViewById(R.id.listView);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        dbcenter = new DatabaseHandler(this);
        db= dbcenter.getReadableDatabase();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpanData();
            }
        });
        Random random = new Random();
        int x = random.nextInt(900) + 100;
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = sdf.format(c.getTime());

        SimpleDateFormat sdfx = new SimpleDateFormat("yyyyddMM");
        String strDatex = sdfx.format(c.getTime());

        kodeorder.setText(strDatex+sharedPrefManager.getID()+x);
        tgl.setText(strDate);
        tgl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });
        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TambahBarangPenjualanActivity.class);
                startActivity(intent);
            }
        });
        hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hapusSemua();
            }
        });
        dataPelanggan();
        dataBarang();
        Total();
        tambahpelanggan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TambahPelangganActivity.class);
                startActivity(intent);
            }
        });
        penginput.setText(sharedPrefManager.getUsername());
    }
    private void Total() {
        cx5 = db.rawQuery("select  sum(total) FROM table_penjualan_detail;",null);
        if(cx5.moveToFirst())
            total.setText(String.valueOf(cx5.getInt(0)));
        else
            total.setText("0");
        cx5.close();
    }
    @Override
    protected void onResume() {
        dataBarang();
        Total();
        dataPelanggan();
        super.onResume();
    }

    private void dataBarang() {
        cursor = db.rawQuery("SELECT * FROM table_penjualan_detail",null);
        cursor.moveToFirst();
        ArrayList<PenjualanDetail> penjualanDetailArrayList = new ArrayList<>();
        for (int i = 0; i < cursor.getCount(); i++) {
            penjualanDetailArrayList.add(new PenjualanDetail(Integer.parseInt(cursor.getString(1)),
                    cursor.getString(2).toString(),
                    cursor.getString(3).toString(),
                    cursor.getString(4).toString(),
                    cursor.getString(5).toString(),
                    cursor.getString(6).toString()));
            cursor.moveToNext();
        }
        penjualanDetailAdapter = new PenjualanDetailListAdapter(KeranjangPenjualanActivity.this, penjualanDetailArrayList,KeranjangPenjualanActivity.this);
        listView.setAdapter(penjualanDetailAdapter);
    }
    private void dataPelanggan() {
        mApiService.getMasterPelanggan()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<PilihPelanggan> pilih = new ArrayList<>();
                                    pilih.add(new PilihPelanggan("0","Pilih Pelanggan"));
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        pilih.add(new PilihPelanggan(datarray.getString("id_pelanggan"),datarray.getString("nama_pelanggan")));
                                    }
                                    ArrayAdapter<PilihPelanggan> adapter = new ArrayAdapter<PilihPelanggan>(KeranjangPenjualanActivity.this, android.R.layout.simple_spinner_dropdown_item,pilih);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    pelanggan.setAdapter(adapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(KeranjangPenjualanActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(KeranjangPenjualanActivity.this, message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(KeranjangPenjualanActivity.this, "Error. Try Again", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void showDateDialog(){
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(KeranjangPenjualanActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tgl.setText(dateFormatter.format(newDate.getTime()));
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
    private void SimpanData() {
        PilihPelanggan pilihPelanggan = (PilihPelanggan) pelanggan.getSelectedItem();
        if (pilihPelanggan.getId().equals("0")){
            Toast.makeText(KeranjangPenjualanActivity.this, "Silahkan Pilih pelanggan terlebih dahulu", Toast.LENGTH_SHORT).show();
        }else if(tgl.getText().toString().equals(""))
        {
            Toast.makeText(KeranjangPenjualanActivity.this,"Tgl tidak boleh kosong",Toast.LENGTH_SHORT).show();
        }else if(total.getText().toString().equals("") | total.getText().toString().equals("0")){
            Toast.makeText(KeranjangPenjualanActivity.this,"Total tidak boleh kosong / 0", Toast.LENGTH_SHORT).show();
        }else{
            mApiService.kirimPenjualan(kodeorder.getText().toString(), pilihPelanggan.getId().toString(),sharedPrefManager.getID(),
                    total.getText().toString(),sharedPrefManager.getUsername(),tgl.getText().toString())
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            if (response.isSuccessful()){
                                try {
                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                    if (jsonRESULTS.getString("code").equals("200")){
                                        String message = jsonRESULTS.getString("message");
                                        cursor = db.rawQuery("SELECT * FROM table_penjualan_detail",null);
                                        cursor.moveToFirst();
                                        for (int i = 1; i <=  cursor.getCount(); i++) {
                                            mApiService.kirimPenjualanDetail(kodeorder.getText().toString(),cursor.getString(1).toString(),
                                                    cursor.getString(4).toString(),
                                                    cursor.getString(5).toString(),
                                                    cursor.getString(6).toString(),
                                                    tgl.getText().toString())
                                                    .enqueue(new Callback<ResponseBody>() {
                                                        @Override
                                                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                                            if (response.isSuccessful()){
                                                                try {
                                                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                                                    if (jsonRESULTS.getString("code").equals("200")){
                                                                        String message = jsonRESULTS.getString("message");

                                                                    } else {
                                                                        String message = jsonRESULTS.getString("message");
                                                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                                                        progressBar.setVisibility(View.INVISIBLE);
                                                                    }
                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                } catch (IOException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            } else {

                                                            }
                                                        }
                                                        @Override
                                                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                            Toast.makeText(getApplicationContext(), "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });

                                            cursor.moveToNext();
                                            if (i==cursor.getCount()){
                                                db.execSQL("DELETE FROM table_penjualan_detail");
                                                dataBarang();
                                                Total();
                                                Toast.makeText(getApplicationContext(), "Penjualan berhasil dilakukan.", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    } else {
                                        String message = jsonRESULTS.getString("message");
                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
//                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });
        }
    }
    public void setSuccessPengajuan(String message) {
        new AlertDialog.Builder(KeranjangPenjualanActivity.this).setTitle("YES").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                db.execSQL("DELETE FROM table_penjualan_detail");
                dataBarang();
                Total();
            }
        }).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void klikhapus(String id) {
        db.execSQL("DELETE FROM table_penjualan_detail where kode_barang = '"+id+"'");
        dataBarang();
        Total();
    }
    public void hapusSemua() {
        db.execSQL("DELETE FROM table_penjualan_detail");
        dataBarang();
        Total();
    }
}
