package com.example.awt;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.adapter.BagGudangAdapter;
import com.example.awt.adapter.KasirAdapter;
import com.example.awt.adapter.PelangganAdapter;
import com.example.awt.adapter.PemesananAdapter;
import com.example.awt.adapter.PimpinanAdapter;
import com.example.awt.adapter.SupplierAdapter;
import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.Karyawan;
import com.example.awt.model.Pelanggan;
import com.example.awt.model.Pemesanan;
import com.example.awt.model.Supplier;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PemesananActivity extends AppCompatActivity implements PemesananAdapter.ClickFunction  {
    ImageView back,tambah;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    TextView nodatatransaksi;
    PemesananAdapter pemesananAdapter;
    private List<Pemesanan> pemesananList;
    ListView listView;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemesanan);
        back =  (ImageView) findViewById(R.id.back);
        tambah =  (ImageView) findViewById(R.id.tambah);
        listView      = (ListView) findViewById(R.id.listView);
        progressBar  = (ProgressBar) findViewById(R.id.progressBar);
        nodatatransaksi  = (TextView) findViewById(R.id.nodatatransaksi);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        progressBar.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TambahPemesananActivity.class);
                startActivity(intent);
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                dataPemesanan();
            }
        },1000);
    }

    private void dataPemesanan() {
        progressBar.setVisibility(View.INVISIBLE);
        mApiService.getPemesanan()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<Pemesanan> pemesananArrayList = new ArrayList<>();

                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        pemesananArrayList.add(new Pemesanan(datarray.getInt("id_pemesanan"),
                                                datarray.getString("kode_pemesanan"),
                                                datarray.getString("nama_supplier"),
                                                datarray.getString("total"),
                                                datarray.getString("pembuat"),
                                                datarray.getString("tgl")));
                                    }
                                    nodatatransaksi.setVisibility(View.INVISIBLE);
                                    if (pemesananArrayList.size()==0){
                                        nodatatransaksi.setVisibility(View.VISIBLE);
                                        nodatatransaksi.setText("Data Pemesanan tidak ada.");
                                    }
                                    pemesananAdapter = new PemesananAdapter(PemesananActivity.this, pemesananArrayList,PemesananActivity.this);
                                    listView.setAdapter(pemesananAdapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(PemesananActivity.this, message, Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(PemesananActivity.this, message, Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.INVISIBLE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(PemesananActivity.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        dataPemesanan();
        super.onResume();
    }


    public void setErrorLoading(String message) {
        new AlertDialog.Builder(PemesananActivity.this).setTitle("UPS").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
    public void setSuccessLoading(String message) {
        new AlertDialog.Builder(PemesananActivity.this).setTitle("Yes").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }

    @Override
    public void klikdetail(String id, String kode, String nama, String total, String pembuat, String tgl) {
        Intent intent = new Intent(getApplicationContext(), PemesananDetailActivity.class);
        intent.putExtra("id",id);
        intent.putExtra("kode",kode);
        intent.putExtra("nama",nama);
        intent.putExtra("total",total);
        intent.putExtra("pembuat",pembuat);
        intent.putExtra("tgl",tgl);
        startActivity(intent);
    }

    @Override
    public void klikhapus(String id) {

    }
}
