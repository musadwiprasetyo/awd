package com.example.awt;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.example.awt.adapter.LaporanPelangganAdapter;
import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;

import com.example.awt.model.LapPelanggan;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LaporanPelangganActivity extends AppCompatActivity implements LaporanPelangganAdapter.ClickFunction  {
    ImageView back;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    TextView nodatatransaksi;
    LaporanPelangganAdapter laporanPelangganAdapter;
    private List<LapPelanggan> pelangganList;
    ListView listView;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_pelanggan);
        back =  (ImageView) findViewById(R.id.back);
        listView      = (ListView) findViewById(R.id.listView);
        progressBar  = (ProgressBar) findViewById(R.id.progressBar);
        nodatatransaksi  = (TextView) findViewById(R.id.nodatatransaksi);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        progressBar.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                data();
            }
        },1000);
    }

    private void data() {
        progressBar.setVisibility(View.INVISIBLE);
        mApiService.getLapPelanggan()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<LapPelanggan> pelangganArrayList = new ArrayList<>();
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        pelangganArrayList.add(new LapPelanggan(datarray.getInt("id_pelanggan"),
                                                datarray.getString("nama"),
                                                datarray.getString("no_hp"),
                                                datarray.getString("alamat")));
                                    }
                                    nodatatransaksi.setVisibility(View.INVISIBLE);
                                    if (pelangganArrayList.size()==0){
                                        nodatatransaksi.setVisibility(View.VISIBLE);
                                        nodatatransaksi.setText("Data pelanggan tidak ada.");
                                    }
                                    laporanPelangganAdapter = new LaporanPelangganAdapter(LaporanPelangganActivity.this, pelangganArrayList,LaporanPelangganActivity.this);
                                    listView.setAdapter(laporanPelangganAdapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(LaporanPelangganActivity.this, message, Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(LaporanPelangganActivity.this, message, Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.INVISIBLE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(LaporanPelangganActivity.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        data();
        super.onResume();
    }
}
