package com.example.awt;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.adapter.BagGudangAdapter;
import com.example.awt.adapter.KasirAdapter;
import com.example.awt.adapter.PelangganAdapter;
import com.example.awt.adapter.PemesananAdapter;
import com.example.awt.adapter.PenjualanAdapter;
import com.example.awt.adapter.PimpinanAdapter;
import com.example.awt.adapter.SupplierAdapter;
import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.Karyawan;
import com.example.awt.model.Pelanggan;
import com.example.awt.model.Pemesanan;
import com.example.awt.model.Penjualan;
import com.example.awt.model.Supplier;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PenjualanActivity extends AppCompatActivity implements PenjualanAdapter.ClickFunction  {
    ImageView back,tambah;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    TextView nodatatransaksi;
    PenjualanAdapter penjualanAdapter;
    private List<Penjualan> penjualanList;
    ListView listView;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penjualan);
        back =  (ImageView) findViewById(R.id.back);
        tambah =  (ImageView) findViewById(R.id.tambah);
        listView      = (ListView) findViewById(R.id.listView);
        progressBar  = (ProgressBar) findViewById(R.id.progressBar);
        nodatatransaksi  = (TextView) findViewById(R.id.nodatatransaksi);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        progressBar.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), KeranjangPenjualanActivity.class);
                startActivity(intent);
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                dataPenjualan();
            }
        },1000);
    }

    private void dataPenjualan() {
        progressBar.setVisibility(View.INVISIBLE);
        mApiService.getPenjualan()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<Penjualan> penjualanArrayList = new ArrayList<>();

                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        penjualanArrayList.add(new Penjualan(datarray.getInt("id_penjualan"),
                                                datarray.getString("kode_penjualan"),
                                                datarray.getString("nama_pelanggan"),
                                                datarray.getString("total"),
                                                datarray.getString("pembuat"),
                                                datarray.getString("tgl")));
                                    }
                                    nodatatransaksi.setVisibility(View.INVISIBLE);
                                    if (penjualanArrayList.size()==0){
                                        nodatatransaksi.setVisibility(View.VISIBLE);
                                        nodatatransaksi.setText("Data Penjualan tidak ada.");
                                    }
                                    penjualanAdapter = new PenjualanAdapter(PenjualanActivity.this, penjualanArrayList,PenjualanActivity.this);
                                    listView.setAdapter(penjualanAdapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(PenjualanActivity.this, message, Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(PenjualanActivity.this, message, Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.INVISIBLE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(PenjualanActivity.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        dataPenjualan();
        super.onResume();
    }


    public void setErrorLoading(String message) {
        new AlertDialog.Builder(PenjualanActivity.this).setTitle("UPS").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
    public void setSuccessLoading(String message) {
        new AlertDialog.Builder(PenjualanActivity.this).setTitle("Yes").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }

    @Override
    public void klikdetail(String id, String kode, String nama, String total, String pembuat, String tgl) {
        Intent intent = new Intent(getApplicationContext(), PenjualanDetailActivity.class);
        intent.putExtra("id",id);
        intent.putExtra("kode",kode);
        intent.putExtra("nama",nama);
        intent.putExtra("total",total);
        intent.putExtra("pembuat",pembuat);
        intent.putExtra("tgl",tgl);
        startActivity(intent);
    }

    @Override
    public void klikhapus(String id) {

    }
}
