package com.example.awt;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.adapter.BagGudangAdapter;
import com.example.awt.adapter.BarangAdapter;
import com.example.awt.adapter.KasirAdapter;
import com.example.awt.adapter.KategoriAdapter;
import com.example.awt.adapter.PimpinanAdapter;
import com.example.awt.adapter.WMAAdapter;
import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.Barang;
import com.example.awt.model.Bulan;
import com.example.awt.model.Karyawan;
import com.example.awt.model.Kategori;
import com.example.awt.model.LaporanWMA;
import com.example.awt.model.PilihKategori;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LaporanWMAActivity extends AppCompatActivity implements WMAAdapter.ClickFunction  {
    ImageView back;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    TextView nodatatransaksi;
    WMAAdapter wmaAdapter;
    private List<LaporanWMA> laporanWMAList;
    ListView listView;
    ProgressBar progressBar;
    Spinner kategori;
    Button show;
    Spinner tahun;
    private String[] Year = {"2020","2021","2022"};
    ArrayAdapter datatahun;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_wma);
        back =  (ImageView) findViewById(R.id.back);
        listView      = (ListView) findViewById(R.id.listView);
        progressBar  = (ProgressBar) findViewById(R.id.progressBar);
        nodatatransaksi  = (TextView) findViewById(R.id.nodatatransaksi);
        kategori = (Spinner) findViewById(R.id.kategori);
        show = (Button) findViewById(R.id.show);
        tahun =  (Spinner) findViewById(R.id.tahun);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        progressBar.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        datatahun = new ArrayAdapter<>(LaporanWMAActivity.this,
                android.R.layout.simple_spinner_dropdown_item,Year);
        tahun.setAdapter(datatahun);
        dataKategori();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                dataWMA();
            }
        },1000);
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PilihKategori pilihkat = (PilihKategori) kategori.getSelectedItem();
                if (pilihkat.getId().equals("0")){
                    Toast.makeText(LaporanWMAActivity.this, "Silahkan Pilih Kategori terlebih dahulu", Toast.LENGTH_SHORT).show();
                }else{
                    dataWMA();
                }
            }
        });
    }
    private void dataKategori() {
        mApiService.getMasterKategori()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<PilihKategori> pilihKategoriArrayList = new ArrayList<>();
                                    pilihKategoriArrayList.add(new PilihKategori("0","0","Pilih Kategori"));
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        pilihKategoriArrayList.add(new PilihKategori(datarray.getString("id_kategori"),datarray.getString("kode_kategori"),datarray.getString("nama_kategori")));
                                    }
                                    ArrayAdapter<PilihKategori> adapter = new ArrayAdapter<PilihKategori>(LaporanWMAActivity.this, android.R.layout.simple_spinner_dropdown_item,pilihKategoriArrayList);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    kategori.setAdapter(adapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(LaporanWMAActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(LaporanWMAActivity.this, message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(LaporanWMAActivity.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void dataWMA() {
        PilihKategori pilihkat = (PilihKategori) kategori.getSelectedItem();
        progressBar.setVisibility(View.INVISIBLE);
        String kategori_id = null;
        if(kategori.getSelectedItem() !=null ) {
            Log.i("kategori",pilihkat.getId());
            kategori_id = String.format(pilihkat.getId());
        }else{
            kategori_id =String.valueOf(0);
        }

        mApiService.getLaporanWMA(kategori_id,tahun.getSelectedItem().toString())
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Log.i("TAG HASIL",response.code()+"");
                            if (response.isSuccessful()) {
                                try {
                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                    if (jsonRESULTS.getString("code").equals("200")) {
                                        String message = jsonRESULTS.getString("message");
                                        JSONArray data = jsonRESULTS.getJSONArray("data");
                                        ArrayList<LaporanWMA> laporanWMAArrayList = new ArrayList<>();
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject datarray = data.getJSONObject(i);
                                            laporanWMAArrayList.add(new LaporanWMA(datarray.getInt("id_rekap"),
                                                    datarray.getString("tahun"),
                                                    datarray.getString("bulan"),
                                                    datarray.getString("penjualan"),
                                                    datarray.getString("wma"),
                                                    datarray.getString("error"),
                                                    datarray.getString("mad")));
                                        }
                                        nodatatransaksi.setVisibility(View.INVISIBLE);
                                        if (laporanWMAArrayList.size()==0){
                                            nodatatransaksi.setVisibility(View.VISIBLE);
                                            nodatatransaksi.setText("Data Laporan WMA tidak ada.");
                                        }
                                        wmaAdapter = new WMAAdapter(LaporanWMAActivity.this, laporanWMAArrayList,LaporanWMAActivity.this);
                                        listView.setAdapter(wmaAdapter);
                                    }else{
                                        String message = jsonRESULTS.getString("message");
                                        Toast.makeText(LaporanWMAActivity.this, message, Toast.LENGTH_SHORT).show();
                                        progressBar.setVisibility(View.INVISIBLE);
                                    }
                                }catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }else{
                                try {
                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(LaporanWMAActivity.this, message, Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.INVISIBLE);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }


                        }
                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.i("TAG","ini terjadi error");
                            Toast.makeText(LaporanWMAActivity.this, "Periksa Koneksi Internet Anda.", Toast.LENGTH_SHORT).show();
                        }
                    });
        }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        dataWMA();
        super.onResume();
    }

    public void setErrorLoading(String message) {
        new AlertDialog.Builder(LaporanWMAActivity.this).setTitle("UPS").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
    public void setSuccessLoading(String message) {
        new AlertDialog.Builder(LaporanWMAActivity.this).setTitle("Yes").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
}
