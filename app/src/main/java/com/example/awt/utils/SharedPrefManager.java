package com.example.awt.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {
    public static final String APP = "AWT MRANGGEN";
    public static final String LOGIN_ID = "id";
    public static final String LOGIN_NAME = "nama";
    public static final String LOGIN_USERNAME = "username";
    public static final String LOGIN_NOHP = "no_hp";
    public static final String LOGIN_AKSES = "akses";
    public static final String LOGIN_AKSES_ID = "akses_id";
    public static final String IS_LOGIN = "statuslogin";


    SharedPreferences SP;
    SharedPreferences.Editor SPEditor;

    public SharedPrefManager(Context context){
        SP = context.getSharedPreferences(APP, Context.MODE_PRIVATE);
        SPEditor = SP.edit();
    }



    public void saveLOGINString(String keyLOGIN, String value){
        SPEditor.putString(keyLOGIN, value);
        SPEditor.commit();
    }

    public void saveLOGINInt(String keyLOGIN, int value){
        SPEditor.putInt(keyLOGIN, value);
        SPEditor.commit();
    }

    public void saveLOGINBoolean(String keyLOGIN, boolean value){
        SPEditor.putBoolean(keyLOGIN, value);
        SPEditor.commit();
    }

    public String getName(){
        return SP.getString(LOGIN_NAME, "");
    }

    public String getID(){
        return SP.getString(LOGIN_ID, "");
    }

    public String getUsername(){
        return SP.getString(LOGIN_USERNAME, "");
    }

    public String getLoginNohp(){
        return SP.getString(LOGIN_NOHP, "");
    }

    public String getLoginAkses(){
        return SP.getString(LOGIN_AKSES, "");
    }
    public String getLoginAksesId(){
        return SP.getString(LOGIN_AKSES_ID, "");
    }

    public Boolean getISLogin(){
        return SP.getBoolean(IS_LOGIN, false);
    }

}
