package com.example.awt.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

    // static variable
    private static final int DATABASE_VERSION = 3;

    // Database name
    private static final String DATABASE_NAME = "AWT";

    // table name
    private static final String table_pemesanan_detail = "table_pemesanan_detail";
    private static final String table_penjualan_detail = "table_penjualan_detail";
    // column tables
//    private static final String KEY_ID = "id";
//    private static final String KEY_PRODUCT_ID = "produk_id";
//    private static final String KEY_PRODUCT_CODE = "produk_code";
//    private static final String KEY_PRODUCT_NAMA = "produk_nama";
//    private static final String KEY_WAREHOUSE = "warehouse_id";
//    private static final String KEY_AWAL = "qty_awal";
//    private static final String KEY_AKHIR = "qty_akhir";

    public DatabaseHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Create table
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table table_pemesanan_detail (id integer primary key, barang_id integer null," +
                "kode_barang text null, nama_barang text null, qty integer null, harga_beli integer null, total integer null);";
        Log.d("Data", "onCreate table_pemesanan_detail: " + sql);
        db.execSQL(sql);

        String sqlriwayat = "create table table_penjualan_detail (id integer primary key, barang_id integer null," +
                "kode_barang text null, nama_barang text null, qty integer null, sub_total integer null, total integer null);";
        Log.d("Data", "onCreate table_penjualan_detail: " + sqlriwayat);
        db.execSQL(sqlriwayat);
    }

    // on Upgrade database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + table_pemesanan_detail);
        db.execSQL("DROP TABLE IF EXISTS " + table_penjualan_detail);
        onCreate(db);
    }
}
