package com.example.awt;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.adapter.BagGudangAdapter;
import com.example.awt.adapter.BarangAdapter;
import com.example.awt.adapter.KasirAdapter;
import com.example.awt.adapter.PimpinanAdapter;
import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.Barang;
import com.example.awt.model.Karyawan;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BarangActivity extends AppCompatActivity implements BarangAdapter.ClickFunction  {
    ImageView back,tambah;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    TextView nodatatransaksi;
    BarangAdapter barangAdapter;
    private List<Barang> barangList;
    ListView listView;
    EditText keyword;
    Button cari;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barang);
        back =  (ImageView) findViewById(R.id.back);
        tambah =  (ImageView) findViewById(R.id.tambah);
        listView      = (ListView) findViewById(R.id.listView);

        keyword      = (EditText) findViewById(R.id.keyword);
        cari         = (Button) findViewById(R.id.cari);

        progressBar  = (ProgressBar) findViewById(R.id.progressBar);
        nodatatransaksi  = (TextView) findViewById(R.id.nodatatransaksi);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        progressBar.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TambahBarangActivity.class);
                startActivity(intent);
            }
        });
        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (keyword.getText().toString().equals("")){
//                    Toast.makeText(BarangActivity.this, "Masukan kode/nama barang terlebih dahulu", Toast.LENGTH_SHORT).show();
//                }else{
                    dataBarang(keyword.getText().toString());
//                }
               // Toast.makeText(BarangActivity.this, keyword.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                dataBarang(keyword.getText().toString());
            }
        },1000);
    }

    private void dataBarang(String key) {
        progressBar.setVisibility(View.INVISIBLE);
        mApiService.getBarang(key)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<Barang> barangArrayList = new ArrayList<>();
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        barangArrayList.add(new Barang(datarray.getInt("id_barang"),
                                                datarray.getString("kategori_id"),
                                                datarray.getString("kode_kategori"),
                                                datarray.getString("nama_kategori"),
                                                datarray.getString("kode_barang"),
                                                datarray.getString("nama_barang"),
                                                datarray.getString("harga_beli"),
                                        datarray.getString("harga_jual"),
                                        datarray.getString("stok"),
                                        datarray.getString("stok_minimal"),
                                        datarray.getString("keterangan"),
                                        datarray.getString("lokasi_rak")));

                                    }
                                    nodatatransaksi.setVisibility(View.INVISIBLE);
                                    if (barangArrayList.size()==0){
                                        nodatatransaksi.setVisibility(View.VISIBLE);
                                        nodatatransaksi.setText("Data Barang tidak ada.");
                                    }
                                    barangAdapter = new BarangAdapter(BarangActivity.this, barangArrayList,BarangActivity.this);
                                    listView.setAdapter(barangAdapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(BarangActivity.this, message, Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(BarangActivity.this, message, Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.INVISIBLE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(BarangActivity.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        dataBarang(keyword.getText().toString());
        super.onResume();
    }


    @Override
    public void klikubah(String id, String kategori_id, String kode_kategori, String nama_kategori, String kode_barang, String nama_barang, String harga_beli, String harga_barang, String stok, String stok_minimal, String keterangan, String lokasi_rak) {
        Intent intent = new Intent(getApplicationContext(), UbahBarangActivity.class);
        intent.putExtra("id",id);
        intent.putExtra("kategori_id",kategori_id);
        intent.putExtra("kode_kategori",kode_kategori);
        intent.putExtra("nama_kategori",nama_kategori);
        intent.putExtra("kode",kode_barang);
        intent.putExtra("nama",nama_barang);
        intent.putExtra("harga_beli",harga_beli);
        intent.putExtra("harga",harga_barang);
        intent.putExtra("stok",stok);
        intent.putExtra("stok_minimal",stok_minimal);
        intent.putExtra("keterangan",keterangan);
        intent.putExtra("lokasi_rak",lokasi_rak);
        startActivity(intent);
    }

    @Override
    public void klikdetail(String id, String kategori_id, String kode_kategori, String nama_kategori, String kode_barang, String nama_barang, String harga_beli, String harga_barang, String stok, String stok_minimal, String keterangan, String lokasi_rak) {
        Intent intent = new Intent(getApplicationContext(), DetailBarangActivity.class);
        intent.putExtra("id",id);
        intent.putExtra("kategori_id",kategori_id);
        intent.putExtra("kode_kategori",kode_kategori);
        intent.putExtra("nama_kategori",nama_kategori);
        intent.putExtra("kode",kode_barang);
        intent.putExtra("nama",nama_barang);
        intent.putExtra("harga_beli",harga_beli);
        intent.putExtra("harga",harga_barang);
        intent.putExtra("stok",stok);
        intent.putExtra("stok_minimal",stok_minimal);
        intent.putExtra("keterangan",keterangan);
        intent.putExtra("lokasi_rak",lokasi_rak);
        startActivity(intent);
    }

    @Override
    public void klikhapus(final String id) {
        new AlertDialog.Builder(BarangActivity.this).setTitle("Apakah anda yakin ingin menghapus data ini?")
                .setCancelable(true)
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    mApiService.hapusBarang(id)
                            .enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                    if (response.isSuccessful()){
                                        try {
                                            JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                            if (jsonRESULTS.getString("code").equals("200")){
                                                String message = jsonRESULTS.getString("message");
                                                setSuccessLoading(message);
                                                dataBarang(keyword.getText().toString());
                                            } else {
                                                String message = jsonRESULTS.getString("message");
                                                setErrorLoading(message);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        setErrorLoading("Gagal menghapus data");
                                    }
                                }
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    setErrorLoading("Periksa Koneksi Internet Anda");
                                }
                            });


            }}).show();
    }
    public void setErrorLoading(String message) {
        new AlertDialog.Builder(BarangActivity.this).setTitle("UPS").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
    public void setSuccessLoading(String message) {
        new AlertDialog.Builder(BarangActivity.this).setTitle("Yes").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
}
