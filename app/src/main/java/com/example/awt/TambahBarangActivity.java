package com.example.awt;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.PilihKategori;
import com.example.awt.model.PilihSupplier;
import com.example.awt.model.Supplier;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahBarangActivity extends AppCompatActivity {
    ImageView back;
    Button simpan;
    EditText nama,harga,hargabeli,keterangan,rak,stokminim;
    Spinner kategori;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    Intent intent;
    ProgressBar progressBar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barang_tambah);
        kategori =  (Spinner) findViewById(R.id.kategori);
        nama =  (EditText) findViewById(R.id.nama);
        harga =  (EditText) findViewById(R.id.harga);
        hargabeli =  (EditText) findViewById(R.id.hargabeli);
        keterangan =  (EditText) findViewById(R.id.keterangan);
        rak =  (EditText) findViewById(R.id.rak);
        stokminim =  (EditText) findViewById(R.id.stokminim);
        simpan =  (Button) findViewById(R.id.simpan);
        back =  (ImageView) findViewById(R.id.back);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        dataKategori();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpanData();
            }
        });
    }
    private void dataKategori() {
        mApiService.getMasterKategori()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<PilihKategori> pilihKategoriArrayList = new ArrayList<>();
                                    pilihKategoriArrayList.add(new PilihKategori("0","0","Pilih Kategori"));
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        pilihKategoriArrayList.add(new PilihKategori(datarray.getString("id_kategori"),datarray.getString("kode_kategori"),datarray.getString("nama_kategori")));
                                    }
                                    ArrayAdapter<PilihKategori> adapter = new ArrayAdapter<PilihKategori>(TambahBarangActivity.this, android.R.layout.simple_spinner_dropdown_item,pilihKategoriArrayList);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    kategori.setAdapter(adapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(TambahBarangActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(TambahBarangActivity.this, message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(TambahBarangActivity.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void SimpanData() {
        PilihKategori pilihkat = (PilihKategori) kategori.getSelectedItem();
        if (pilihkat.getId().equals("0")){
            Toast.makeText(TambahBarangActivity.this, "Silahkan Pilih Kategori terlebih dahulu", Toast.LENGTH_SHORT).show();
        }
        else if (nama.getText().toString().equals(""))
        {
            Toast.makeText(TambahBarangActivity.this,"Nama tidak boleh kosong",Toast.LENGTH_SHORT).show();
        }else if(stokminim.getText().toString().equals("") | stokminim.getText().toString().equals("0")){
            Toast.makeText(TambahBarangActivity.this,"Stok Minimal tidak boleh kosong / 0", Toast.LENGTH_SHORT).show();
        }else if(harga.getText().toString().equals("")){
            Toast.makeText(TambahBarangActivity.this,"Harga tidak boleh kosong", Toast.LENGTH_SHORT).show();
        }else{
            progressBar.setVisibility(View.VISIBLE);
            mApiService.simpanBarang(pilihkat.getId().toString(),pilihkat.getKode().toString(),nama.getText().toString(),
                    hargabeli.getText().toString(),
                    harga.getText().toString(),
                    stokminim.getText().toString(),
                    keterangan.getText().toString(),
                    rak.getText().toString())
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            if (response.isSuccessful()){
                                try {
                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                    if (jsonRESULTS.getString("code").equals("200")){
                                        String message = jsonRESULTS.getString("message");
                                        onBackPressed();
                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                    } else {
                                        String message = jsonRESULTS.getString("message");
                                        Log.e("ERROR",message);
                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                        progressBar.setVisibility(View.INVISIBLE);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.INVISIBLE);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
