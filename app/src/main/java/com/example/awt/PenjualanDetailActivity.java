package com.example.awt;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.adapter.BagGudangAdapter;
import com.example.awt.adapter.KasirAdapter;
import com.example.awt.adapter.PelangganAdapter;
import com.example.awt.adapter.PemesananAdapter;
import com.example.awt.adapter.PemesananDetailAdapter;
import com.example.awt.adapter.PenjualanDetailAdapter;
import com.example.awt.adapter.PimpinanAdapter;
import com.example.awt.adapter.SupplierAdapter;
import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.Karyawan;
import com.example.awt.model.Pelanggan;
import com.example.awt.model.Pemesanan;
import com.example.awt.model.PemesananDetail;
import com.example.awt.model.PenjualanDetail;
import com.example.awt.model.Supplier;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PenjualanDetailActivity extends AppCompatActivity implements PenjualanDetailAdapter.ClickFunction  {
    ImageView back;
    Button cetak;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    TextView nodatatransaksi;
    PenjualanDetailAdapter penjualanDetailAdapter;
    private List<PenjualanDetail> penjualanDetailList;
    ListView listView;
    ProgressBar progressBar;
    TextView kode,pelanggan,tgl,penginput,total;
    Intent data_detail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penjualan_detail);
        back =  (ImageView) findViewById(R.id.back);
        cetak      = (Button) findViewById(R.id.cetak);

        listView      = (ListView) findViewById(R.id.listView);
        progressBar  = (ProgressBar) findViewById(R.id.progressBar);
        nodatatransaksi  = (TextView) findViewById(R.id.nodatatransaksi);

        kode      = (TextView) findViewById(R.id.kode);
        pelanggan  = (TextView) findViewById(R.id.pelanggan);
        tgl       = (TextView) findViewById(R.id.tgl);
        penginput = (TextView) findViewById(R.id.penginput);
        total     = (TextView) findViewById(R.id.total);

        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        progressBar.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        data_detail = getIntent();

        kode.setText(data_detail.getStringExtra("kode"));
        pelanggan.setText(data_detail.getStringExtra("nama"));
        tgl.setText(data_detail.getStringExtra("tgl"));
        penginput.setText(data_detail.getStringExtra("pembuat"));
        total.setText(data_detail.getStringExtra("total"));
        cetak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(PenjualanDetailActivity.this, "Berhasil dicetak.", Toast.LENGTH_SHORT).show();
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                dataPenjualanDetail();
            }
        },1000);
    }

    private void dataPenjualanDetail() {
        progressBar.setVisibility(View.INVISIBLE);
        mApiService.penjualanDetail(data_detail.getStringExtra("id"))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<PenjualanDetail> penjualanDetailArrayList = new ArrayList<>();

                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        penjualanDetailArrayList.add(new PenjualanDetail(datarray.getInt("id_penjualan_detail"),
                                                datarray.getString("kode"),
                                                datarray.getString("nama"),
                                                datarray.getString("qty"),
                                                datarray.getString("sub_total"),
                                                datarray.getString("total")));
                                    }
                                    nodatatransaksi.setVisibility(View.INVISIBLE);
                                    if (penjualanDetailArrayList.size()==0){
                                        nodatatransaksi.setVisibility(View.VISIBLE);
                                        nodatatransaksi.setText("Data Penjualan Detail tidak ada.");
                                    }
                                    penjualanDetailAdapter = new PenjualanDetailAdapter(PenjualanDetailActivity.this, penjualanDetailArrayList,PenjualanDetailActivity.this);
                                    listView.setAdapter(penjualanDetailAdapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(PenjualanDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(PenjualanDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.INVISIBLE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(PenjualanDetailActivity.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        dataPenjualanDetail();
        super.onResume();
    }


    public void setErrorLoading(String message) {
        new AlertDialog.Builder(PenjualanDetailActivity.this).setTitle("UPS").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
    public void setSuccessLoading(String message) {
        new AlertDialog.Builder(PenjualanDetailActivity.this).setTitle("Yes").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }

}
