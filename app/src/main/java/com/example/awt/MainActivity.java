package com.example.awt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.PenjualanDetail;
import com.example.awt.utils.SharedPrefManager;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    TextView nama,namaakses;
    Button logout;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    String namakaryawan, aksesnama;
    private Boolean islogin;
    Button karyawan,supplier,pelanggan,barang,pembelian,penjualan,wma,lapwma,pimpinan,gudang,kasir;
    Intent intents;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nama =  (TextView) findViewById(R.id.nama);
        namaakses =  (TextView) findViewById(R.id.namaakses);
        logout =  (Button) findViewById(R.id.logout);
        karyawan =  (Button) findViewById(R.id.karyawan);
        pembelian =  (Button) findViewById(R.id.pembelian);
        supplier =  (Button) findViewById(R.id.supplier);
        pelanggan =  (Button) findViewById(R.id.pelanggan);
        barang =  (Button) findViewById(R.id.barang);
        wma =  (Button) findViewById(R.id.wma);
        lapwma =  (Button) findViewById(R.id.lapwma);

        pembelian =  (Button) findViewById(R.id.pembelian);
        penjualan =  (Button) findViewById(R.id.penjualan);
        pimpinan =  (Button) findViewById(R.id.pimpinan);
        gudang =  (Button) findViewById(R.id.gudang);
        kasir =  (Button) findViewById(R.id.kasir);
//        supplier =  (Button) findViewById(R.id.supplier);
//        pelanggan =  (Button) findViewById(R.id.pelanggan);
//        barang =  (Button) findViewById(R.id.barang);
//        kategori =  (Button) findViewById(R.id.kategori);
//        pemesanan =  (Button) findViewById(R.id.pemesanan);
//        penjualan =  (Button) findViewById(R.id.penjualan);
//        barangmasuk =  (Button) findViewById(R.id.barangmasuk);
//        barangkeluar =  (Button) findViewById(R.id.barangkeluar);
//        wma =  (Button) findViewById(R.id.wma);
//
//        laporan =  (Button) findViewById(R.id.laporan);


        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        islogin = sharedPrefManager.getISLogin();
        if(islogin == false ){
            Intent intent = new Intent(MainActivity.this,
                    LoginActivity.class);
            startActivity(intent);
        }
        nama.setText(sharedPrefManager.getName());
        namaakses.setText(sharedPrefManager.getLoginAkses());
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                keluar();
            }
        });
        karyawan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanKaryawanActivity.class);
                startActivity(intent);
            }
        });

        supplier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanSupplierActivity.class);
                startActivity(intent);
            }
        });
        pelanggan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanPelangganActivity.class);
                startActivity(intent);
            }
        });
        barang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanBarangActivity.class);
                startActivity(intent);
            }
        });
        pembelian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanPembelianActivity.class);
                startActivity(intent);
            }
        });
        penjualan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanPenjualanActivity.class);
                startActivity(intent);
            }
        });
        lapwma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanWMAActivity.class);
                startActivity(intent);
            }
        });
        wma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FormWMAActivity.class);
                startActivity(intent);
            }
        });
        pimpinan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PimpinanActivity.class);
                startActivity(intent);
            }
        });
        gudang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), BagGudangActivity.class);
                startActivity(intent);
            }
        });
        kasir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), KasirActivity.class);
                startActivity(intent);
            }
        });
//        supplier.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), SupplierActivity.class);
//                startActivity(intent);
//            }
//        });
//        pelanggan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), PelangganActivity.class);
//                startActivity(intent);
//            }
//        });
//        kategori.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), KategoriActivity.class);
//                startActivity(intent);
//            }
//        });
//        barang.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), BarangActivity.class);
//                startActivity(intent);
//            }
//        });
//        pemesanan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), PemesananActivity.class);
//                startActivity(intent);
//            }
//        });
//        penjualan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), PenjualanActivity.class);
//                startActivity(intent);
//            }
//        });
//        barangmasuk.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), BarangMasukActivity.class);
//                startActivity(intent);
//            }
//        });
//        barangkeluar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), BarangKeluarActivity.class);
//                startActivity(intent);
//            }
//        });
//        wma.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), FormWMAActivity.class);
//                startActivity(intent);
//            }
//        });
//
//        laporan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), LaporanActivity.class);
//                startActivity(intent);
//            }
//        });
    }
    private void keluar() {
        sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_ID,"");
        sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_NAME,"");
        sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_USERNAME,"");
        sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_AKSES,"");
        sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_NOHP,"");
        sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_AKSES_ID,"");
        sharedPrefManager.saveLOGINBoolean(SharedPrefManager.IS_LOGIN, false);
        Toast.makeText(getApplicationContext(), "Berhasil keluar dari aplikasi ", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}