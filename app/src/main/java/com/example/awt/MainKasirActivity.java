package com.example.awt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.utils.SharedPrefManager;

import org.w3c.dom.Text;

public class MainKasirActivity extends AppCompatActivity {
    TextView nama,namaakses;
    Button logout,supplier, pemesanan, penjualan,pelanggan,barangmasuk,barangkeluar,wma,lapwma;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    String namakaryawan, aksesnama;
    private Boolean islogin;
    Intent intents;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_kasir);
        nama =  (TextView) findViewById(R.id.nama);
        namaakses =  (TextView) findViewById(R.id.namaakses);
        logout =  (Button) findViewById(R.id.logout);
        supplier =  (Button) findViewById(R.id.supplier);
        pemesanan =  (Button) findViewById(R.id.pemesanan);

        penjualan =  (Button) findViewById(R.id.penjualan);
        pelanggan =  (Button) findViewById(R.id.pelanggan);
        barangmasuk =  (Button) findViewById(R.id.barangmasuk);
        barangkeluar =  (Button) findViewById(R.id.barangkeluar);
        wma =  (Button) findViewById(R.id.wma);
        lapwma =  (Button) findViewById(R.id.lapwma);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        islogin = sharedPrefManager.getISLogin();
        if(islogin == false ){
            Intent intent = new Intent(MainKasirActivity.this,
                    LoginActivity.class);
            startActivity(intent);
        }
        nama.setText(sharedPrefManager.getName());
        namaakses.setText(sharedPrefManager.getLoginAkses());
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                keluar();
            }
        });

        supplier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SupplierActivity.class);
                startActivity(intent);
            }
        });
        pemesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PemesananActivity.class);
                startActivity(intent);
            }
        });


        penjualan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PenjualanActivity.class);
                startActivity(intent);
            }
        });
        pelanggan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PelangganActivity.class);
                startActivity(intent);
            }
        });
        barangmasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), BarangMasukActivity.class);
                startActivity(intent);
            }
        });
        barangkeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), BarangKeluarActivity.class);
                startActivity(intent);
            }
        });
        wma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FormWMAActivity.class);
                startActivity(intent);
            }
        });
        lapwma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LaporanWMAActivity.class);
                startActivity(intent);
            }
        });
    }
    private void keluar() {
        sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_ID,"");
        sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_NAME,"");
        sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_USERNAME,"");
        sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_AKSES,"");
        sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_NOHP,"");
        sharedPrefManager.saveLOGINString(SharedPrefManager.LOGIN_AKSES_ID,"");
        sharedPrefManager.saveLOGINBoolean(SharedPrefManager.IS_LOGIN, false);
        Toast.makeText(getApplicationContext(), "Berhasil keluar dari aplikasi ", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}