package com.example.awt;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.adapter.MasterBarangAdapter;
import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.BarangTambah;
import com.example.awt.utils.DatabaseHandler;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahBarangPenjualanActivity extends AppCompatActivity implements MasterBarangAdapter.ClickFunction  {
    ImageView back,tambah;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    TextView nodatatransaksi;
    MasterBarangAdapter masterBarangAdapter;
    private List<BarangTambah> barangList;
    ListView listView;
    ProgressBar progressBar;
    Button tambahpelanggan;
    DatabaseHandler dbcenter;
    SQLiteDatabase db;
    String key="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barang_master);
        back =  (ImageView) findViewById(R.id.back);
        tambah =  (ImageView) findViewById(R.id.tambah);
        listView      = (ListView) findViewById(R.id.listView);
        progressBar  = (ProgressBar) findViewById(R.id.progressBar);

        nodatatransaksi  = (TextView) findViewById(R.id.nodatatransaksi);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        progressBar.setVisibility(View.VISIBLE);
        dbcenter = new DatabaseHandler(this);
        db= dbcenter.getReadableDatabase();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.INVISIBLE);
                dataBarang("");
            }
        },1000);


    }

    private void dataBarang(String s) {
        progressBar.setVisibility(View.INVISIBLE);
        mApiService.getBarang("")
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<BarangTambah> barangArrayList = new ArrayList<>();
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        barangArrayList.add(new BarangTambah(datarray.getInt("id_barang"),
                                                datarray.getString("kode_barang"),
                                                datarray.getString("nama_barang"),
                                                datarray.getString("harga_jual"),
                                                datarray.getString("stok"),
                                                datarray.getString("keterangan"),
                                                datarray.getString("lokasi_rak")));
                                    }
                                    nodatatransaksi.setVisibility(View.INVISIBLE);
                                    if (barangArrayList.size()==0){
                                        nodatatransaksi.setVisibility(View.VISIBLE);
                                        nodatatransaksi.setText("Data Barang tidak ada.");
                                    }
                                    masterBarangAdapter = new MasterBarangAdapter(TambahBarangPenjualanActivity.this, barangArrayList,TambahBarangPenjualanActivity.this);
                                    listView.setAdapter(masterBarangAdapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(TambahBarangPenjualanActivity.this, message, Toast.LENGTH_SHORT).show();
                                    progressBar.setVisibility(View.INVISIBLE);
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(TambahBarangPenjualanActivity.this, message, Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.INVISIBLE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(TambahBarangPenjualanActivity.this, "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        dataBarang("");
        super.onResume();
    }
    @Override
    public void klikpilih(String id, String kode, String nama, String harga,String stok) {
        Cursor cursor = db.rawQuery("SELECT * FROM table_penjualan_detail WHERE barang_id = '"+id+"' " +
                "and kode_barang = '"+kode+"' Limit 1;", null);
        int qty =1;
        if (cursor.moveToFirst()) {
            int totalqty  = Integer.parseInt(cursor.getString(4).toString()) + qty;
            int totalvalue = totalqty * Integer.parseInt(harga) ;
            if (Integer.parseInt(stok)>= totalqty){
                db.execSQL("UPDATE table_penjualan_detail SET qty = '"+totalqty+"',sub_total='"+harga+"',total='"+totalvalue+"'" +
                        "WHERE barang_id = '"+id+"' and kode_barang = '"+kode+"' ");
                Log.d("Data", "onCreate: UPDATE QTY " + totalqty);
                Toast.makeText(TambahBarangPenjualanActivity.this, "Berhasil mengupdate nilai qty barang di keranjang penjualan", Toast.LENGTH_SHORT).show();

            }else{
                Toast.makeText(TambahBarangPenjualanActivity.this, "Stok tidak memenuhi", Toast.LENGTH_SHORT).show();
            }

        }else {
            int totalvalue = qty * Integer.parseInt(harga);

            db.execSQL("insert into table_penjualan_detail(barang_id, kode_barang,nama_barang, qty, sub_total,total) values('" +
                    id + "','" +
                    kode + "','" +
                    nama + "','" +
                    qty + "','" +
                    harga + "','" +
                    totalvalue + "')");
            Log.d("Data", "onCreate: SIMPAN KE KERANJANG");
            Toast.makeText(TambahBarangPenjualanActivity.this, "Berhasil memasukan barang di keranjang penjualan", Toast.LENGTH_SHORT).show();
        }
    }
    public void setErrorLoading(String message) {
        new AlertDialog.Builder(TambahBarangPenjualanActivity.this).setTitle("UPS").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
    public void setSuccessLoading(String message) {
        new AlertDialog.Builder(TambahBarangPenjualanActivity.this).setTitle("Yes").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }
}