package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.Karyawan;

import java.util.ArrayList;
import java.util.List;

public class PimpinanAdapter extends ArrayAdapter<Karyawan> {

        List<Karyawan> karyawanList;
        Context context;
        private LayoutInflater mInflater;
        private ArrayList<Karyawan> arraylist;
        private PimpinanAdapter.ClickFunction listener;
        public interface ClickFunction {
            void klikubah(String id,String nama, String nohp, String username);
            void klikhapus(String id);
        }
        // Constructors
        public PimpinanAdapter(Context context, List<Karyawan> objects, PimpinanAdapter.ClickFunction listener) {
            super(context, 0, objects);
            this.context = context;
            this.mInflater = LayoutInflater.from(context);
            karyawanList = objects;
            this.arraylist = new ArrayList<Karyawan>();
            this.arraylist.addAll(karyawanList);
            this.listener  = listener;
        }
        @Override
        public int getCount() {
            return karyawanList.size();
        }

         @Override
         public Karyawan getItem(int position) {
            return karyawanList.get(position);
         }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.pimpinan_list_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final Karyawan item = getItem(position);
        vh.ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikubah(String.valueOf(item.getId_karyawan()),String.valueOf(item.getNama()),String.valueOf(item.getNo_hp()),String.valueOf(item.getUsername()));
                //Toast.makeText(getContext(),String.valueOf(item.getId()),Toast.LENGTH_SHORT).show();
            }
        });
        vh.hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikhapus(String.valueOf(item.getId_karyawan()));
                //Toast.makeText(getContext(),String.valueOf(item.getId()),Toast.LENGTH_SHORT).show();
            }
        });
        vh.username.setText(item.getUsername());
        vh.nama.setText(item.getNama());
        vh.nohp.setText(item.getNo_hp());
        return vh.rootView;
    }

private static class ViewHolder {
    public final RelativeLayout rootView;
    public final TextView username,nama,nohp,ubah,hapus;


    private ViewHolder(RelativeLayout rootView, TextView username, TextView nama,TextView nohp, TextView ubah,TextView hapus) {
        this.rootView = rootView;
        this.username = username;
        this.nama = nama;
        this.nohp = nohp;
        this.ubah = ubah;
        this.hapus = hapus;
    }
    public static ViewHolder create(RelativeLayout rootView) {

        TextView username  = (TextView) rootView.findViewById(R.id.username);
        TextView nama = (TextView) rootView.findViewById(R.id.nama);
        TextView nohp = (TextView) rootView.findViewById(R.id.nohp);
        TextView ubah = (TextView) rootView.findViewById(R.id.ubah);
        TextView hapus = (TextView) rootView.findViewById(R.id.hapus);
        return new ViewHolder (rootView,username,nama,nohp,ubah,hapus);
    }
    }
}
