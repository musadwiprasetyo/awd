package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.LapSupplier;


import java.util.ArrayList;
import java.util.List;

public class LaporanSupplierAdapter extends ArrayAdapter<LapSupplier> {

    List<LapSupplier> lapSupplierList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<LapSupplier> arraylist;
    private LaporanSupplierAdapter.ClickFunction listener;
    public interface ClickFunction {

    }
    // Constructors
    public LaporanSupplierAdapter(Context context, List<LapSupplier> objects, LaporanSupplierAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        lapSupplierList = objects;
        this.arraylist = new ArrayList<LapSupplier>();
        this.arraylist.addAll(lapSupplierList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return lapSupplierList.size();
    }

    @Override
    public LapSupplier getItem(int position) {
        return lapSupplierList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LaporanSupplierAdapter.ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.laporan_supplier_list_view, parent, false);
            vh = LaporanSupplierAdapter.ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (LaporanSupplierAdapter.ViewHolder) convertView.getTag();
        }
        final LapSupplier item = getItem(position);

        vh.id.setText(String.valueOf(item.getId_supplier()));
        vh.nama.setText(item.getNama());
        vh.alamat.setText(item.getAlamat());
        vh.hp.setText(item.getNo_hp());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView id,nama,alamat,hp;

        private ViewHolder(RelativeLayout rootView, TextView id, TextView nama,TextView alamat,TextView hp) {
            this.rootView = rootView;

            this.id = id;
            this.nama = nama;
            this.alamat = alamat;
            this.hp = hp;
        }
        public static LaporanSupplierAdapter.ViewHolder create(RelativeLayout rootView) {
            TextView id  = (TextView) rootView.findViewById(R.id.id);
            TextView nama = (TextView) rootView.findViewById(R.id.nama);
            TextView alamat = (TextView) rootView.findViewById(R.id.alamat);
            TextView hp = (TextView) rootView.findViewById(R.id.hp);
            return new LaporanSupplierAdapter.ViewHolder(rootView,id,nama,alamat,hp);
        }
    }
}
