package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.LapPenjualan;


import java.util.ArrayList;
import java.util.List;

public class LaporanPenjualanAdapter extends ArrayAdapter<LapPenjualan> {

    List<LapPenjualan> penjualanList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<LapPenjualan> arraylist;
    private LaporanPenjualanAdapter.ClickFunction listener;
    public interface ClickFunction {

    }
    // Constructors
    public LaporanPenjualanAdapter(Context context, List<LapPenjualan> objects, LaporanPenjualanAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        penjualanList = objects;
        this.arraylist = new ArrayList<LapPenjualan>();
        this.arraylist.addAll(penjualanList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return penjualanList.size();
    }

    @Override
    public LapPenjualan getItem(int position) {
        return penjualanList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LaporanPenjualanAdapter.ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.laporan_penjualan_list_view, parent, false);
            vh = LaporanPenjualanAdapter.ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (LaporanPenjualanAdapter.ViewHolder) convertView.getTag();
        }
        final LapPenjualan item = getItem(position);

        vh.id.setText(String.valueOf(position+1));
        vh.nota.setText(String.valueOf(item.getKode_penjualan()));
        vh.tgl.setText(item.getTgl());
        vh.pelanggan.setText(item.getNama_pelanggan());
        vh.oleh.setText("Oleh : "+item.getPembuat());

        vh.barang.setText(item.getKodenama());
        vh.qty.setText(item.getQtysubtotal());
        vh.total.setText(item.getTotal());

        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView id,nota,tgl,pelanggan,oleh,barang,qty,total;

        private ViewHolder(RelativeLayout rootView, TextView id, TextView nota,TextView tgl,TextView pelanggan,TextView oleh,TextView barang,TextView qty,TextView total) {
            this.rootView = rootView;

            this.id = id;
            this.nota = nota;
            this.tgl = tgl;
            this.pelanggan = pelanggan;
            this.oleh = oleh;
            this.barang = barang;
            this.qty = qty;
            this.total = total;
        }
        public static LaporanPenjualanAdapter.ViewHolder create(RelativeLayout rootView) {
            TextView id  = (TextView) rootView.findViewById(R.id.id);
            TextView nota = (TextView) rootView.findViewById(R.id.nota);
            TextView tgl = (TextView) rootView.findViewById(R.id.tgl);
            TextView pelanggan = (TextView) rootView.findViewById(R.id.pelanggan);
            TextView oleh = (TextView) rootView.findViewById(R.id.oleh);
            TextView barang = (TextView) rootView.findViewById(R.id.barang);
            TextView qty = (TextView) rootView.findViewById(R.id.qty);
            TextView total = (TextView) rootView.findViewById(R.id.total);
            return new LaporanPenjualanAdapter.ViewHolder(rootView,id,nota,tgl,pelanggan,oleh,barang,qty,total);
        }
    }
}
