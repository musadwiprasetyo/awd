package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.Kategori;
import com.example.awt.model.LaporanWMA;

import java.util.ArrayList;
import java.util.List;

public class WMAAdapter extends ArrayAdapter<LaporanWMA> {

    List<LaporanWMA> laporanWMAList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<LaporanWMA> arraylist;
    private WMAAdapter.ClickFunction listener;
    public interface ClickFunction {

    }
    // Constructors
    public WMAAdapter(Context context, List<LaporanWMA> objects, WMAAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        laporanWMAList = objects;
        this.arraylist = new ArrayList<LaporanWMA>();
        this.arraylist.addAll(laporanWMAList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return laporanWMAList.size();
    }

    @Override
    public LaporanWMA getItem(int position) {
        return laporanWMAList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.laporan_wma_list_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final LaporanWMA item = getItem(position);

        vh.bulan.setText(item.getBulan());
        vh.penjualan.setText(item.getPenjualan());
        vh.wma.setText(item.getWma());
        vh.error.setText(item.getError());
        vh.mad.setText(item.getMad());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView bulan,penjualan,wma,error,mad;


        private ViewHolder(RelativeLayout rootView, TextView bulan, TextView penjualan,TextView wma,TextView error,TextView mad) {
            this.rootView = rootView;
            this.bulan = bulan;
            this.penjualan = penjualan;
            this.wma = wma;
            this.error = error;
            this.mad = mad;
        }
        public static ViewHolder create(RelativeLayout rootView) {

            TextView bulan  = (TextView) rootView.findViewById(R.id.bulan);
            TextView penjualan = (TextView) rootView.findViewById(R.id.penjualan);
            TextView wma = (TextView) rootView.findViewById(R.id.wma);
            TextView error = (TextView) rootView.findViewById(R.id.error);
            TextView mad = (TextView) rootView.findViewById(R.id.mad);
            return new ViewHolder (rootView,bulan,penjualan,wma,error,mad);
        }
    }
}
