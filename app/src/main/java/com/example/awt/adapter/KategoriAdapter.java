package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.Kategori;

import java.util.ArrayList;
import java.util.List;

public class KategoriAdapter extends ArrayAdapter<Kategori> {

    List<Kategori> kategoriList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<Kategori> arraylist;
    private KategoriAdapter.ClickFunction listener;
    public interface ClickFunction {
        void klikubah(String id,String kode, String nama);
        void klikhapus(String id);
    }
    // Constructors
    public KategoriAdapter(Context context, List<Kategori> objects, KategoriAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        kategoriList = objects;
        this.arraylist = new ArrayList<Kategori>();
        this.arraylist.addAll(kategoriList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return kategoriList.size();
    }

    @Override
    public Kategori getItem(int position) {
        return kategoriList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.kategori_list_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final Kategori item = getItem(position);
        vh.ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikubah(String.valueOf(item.getId_kategori()),String.valueOf(item.getKode_kategori()),String.valueOf(item.getNama_kategori()));
            }
        });
        vh.hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikhapus(String.valueOf(item.getId_kategori()));
            }
        });
        vh.kode.setText(item.getKode_kategori());
        vh.nama.setText(item.getNama_kategori());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView kode,nama,ubah,hapus;


        private ViewHolder(RelativeLayout rootView, TextView kode, TextView nama,TextView ubah,TextView hapus) {
            this.rootView = rootView;
            this.kode = kode;
            this.nama = nama;
            this.ubah = ubah;
            this.hapus = hapus;
        }
        public static ViewHolder create(RelativeLayout rootView) {

            TextView kode  = (TextView) rootView.findViewById(R.id.kode);
            TextView nama = (TextView) rootView.findViewById(R.id.nama);
            TextView ubah = (TextView) rootView.findViewById(R.id.ubah);
            TextView hapus = (TextView) rootView.findViewById(R.id.hapus);
            return new ViewHolder (rootView,kode,nama,ubah,hapus);
        }
    }
}
