package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.Karyawan;
import com.example.awt.model.Pelanggan;
import com.example.awt.model.Pemesanan;

import java.util.ArrayList;
import java.util.List;

public class PemesananAdapter extends ArrayAdapter<Pemesanan> {

    List<Pemesanan> pemesananList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<Pemesanan> arraylist;
    private PemesananAdapter.ClickFunction listener;
    public interface ClickFunction {
        void klikdetail(String id,String kode, String nama, String total, String pembuat, String tgl);
        void klikhapus(String id);
    }
    // Constructors
    public PemesananAdapter(Context context, List<Pemesanan> objects, PemesananAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        pemesananList = objects;
        this.arraylist = new ArrayList<Pemesanan>();
        this.arraylist.addAll(pemesananList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return pemesananList.size();
    }

    @Override
    public Pemesanan getItem(int position) {
        return pemesananList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.pemesanan_list_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final Pemesanan item = getItem(position);
        vh.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikdetail(String.valueOf(item.getId_pemesanan()),
                        String.valueOf(item.getKode_pemesanan()),String.valueOf(item.getNama_supplier()),
                        String.valueOf(item.getTotal()),
                        String.valueOf(item.getPembuat()),
                        String.valueOf(item.getTgl()));
            }
        });

        vh.kode.setText(item.getKode_pemesanan());
        vh.supplier.setText(item.getNama_supplier());
        vh.tgl.setText(item.getTgl());
        vh.total.setText(item.getTotal());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView tgl,kode,total,supplier,detail;


        private ViewHolder(RelativeLayout rootView, TextView tgl, TextView kode,TextView total, TextView supplier,TextView detail) {
            this.rootView = rootView;
            this.tgl = tgl;
            this.kode = kode;
            this.total = total;
            this.supplier = supplier;
            this.detail = detail;
        }
        public static ViewHolder create(RelativeLayout rootView) {

            TextView tgl  = (TextView) rootView.findViewById(R.id.tgl);
            TextView kode = (TextView) rootView.findViewById(R.id.kode);
            TextView total = (TextView) rootView.findViewById(R.id.total);
            TextView supplier = (TextView) rootView.findViewById(R.id.supplier);
            TextView detail = (TextView) rootView.findViewById(R.id.detail);
            return new ViewHolder (rootView,tgl,kode,total,supplier,detail);
        }
    }
}
