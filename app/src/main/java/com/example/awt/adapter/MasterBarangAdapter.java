package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.Barang;
import com.example.awt.model.BarangTambah;
import com.example.awt.model.Karyawan;

import java.util.ArrayList;
import java.util.List;

public class MasterBarangAdapter extends ArrayAdapter<BarangTambah> {

    List<BarangTambah> barangList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<BarangTambah> arraylist;
    private MasterBarangAdapter.ClickFunction listener;
    public interface ClickFunction {
        void klikpilih(String id,String kode, String nama, String harga, String stok);
    }
    // Constructors
    public MasterBarangAdapter(Context context, ArrayList<BarangTambah> objects, ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        barangList = objects;
        this.arraylist = new ArrayList<BarangTambah>();
        this.arraylist.addAll(barangList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return barangList.size();
    }

    @Override
    public BarangTambah getItem(int position) {
        return barangList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.master_barang_list_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final BarangTambah item = getItem(position);
        vh.pilih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikpilih(String.valueOf(item.getId_barang()),
                        String.valueOf(item.getKode_barang()),
                        String.valueOf(item.getNama_barang()),
                        String.valueOf(item.getHarga_barang()),
                        String.valueOf(item.getStok()));
            }
        });
        vh.kode.setText(item.getKode_barang());
        vh.nama.setText(item.getNama_barang());
        vh.harga.setText(item.getHarga_barang());
        vh.stok.setText(item.getStok());
        vh.keterangan.setText(item.getKeterangan());
        vh.rak.setText(item.getRak());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView kode,nama,harga,stok,keterangan,rak,pilih;


        private ViewHolder(RelativeLayout rootView, TextView kode, TextView nama,TextView harga,TextView stok,TextView keterangan,TextView rak, TextView pilih) {
            this.rootView = rootView;
            this.kode = kode;
            this.nama = nama;
            this.harga = harga;
            this.stok = stok;
            this.keterangan = keterangan;
            this.rak = rak;
            this.pilih = pilih;
        }
        public static ViewHolder create(RelativeLayout rootView) {

            TextView kode  = (TextView) rootView.findViewById(R.id.kode);
            TextView nama = (TextView) rootView.findViewById(R.id.nama);
            TextView harga = (TextView) rootView.findViewById(R.id.harga);
            TextView stok = (TextView) rootView.findViewById(R.id.stok);
            TextView keterangan = (TextView) rootView.findViewById(R.id.keterangan);
            TextView rak = (TextView) rootView.findViewById(R.id.rak);

            TextView pilih = (TextView) rootView.findViewById(R.id.pilih);
            return new ViewHolder (rootView,kode,nama,harga,stok,keterangan,rak,pilih);
        }
    }
}
