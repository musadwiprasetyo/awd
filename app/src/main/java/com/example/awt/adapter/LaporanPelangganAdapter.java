package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.LapPelanggan;

import java.util.ArrayList;
import java.util.List;

public class LaporanPelangganAdapter extends ArrayAdapter<LapPelanggan> {

    List<LapPelanggan> pelangganList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<LapPelanggan> arraylist;
    private LaporanPelangganAdapter.ClickFunction listener;
    public interface ClickFunction {

    }
    // Constructors
    public LaporanPelangganAdapter(Context context, List<LapPelanggan> objects, LaporanPelangganAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        pelangganList = objects;
        this.arraylist = new ArrayList<LapPelanggan>();
        this.arraylist.addAll(pelangganList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return pelangganList.size();
    }

    @Override
    public LapPelanggan getItem(int position) {
        return pelangganList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LaporanPelangganAdapter.ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.laporan_pelanggan_list_view, parent, false);
            vh = LaporanPelangganAdapter.ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (LaporanPelangganAdapter.ViewHolder) convertView.getTag();
        }
        final LapPelanggan item = getItem(position);

        vh.id.setText(String.valueOf(item.getId_pelanggan()));
        vh.nama.setText(item.getNama_pelanggan());
        vh.alamat.setText(item.getAlamat());
        vh.hp.setText(item.getNo_hp());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView id,nama,alamat,hp;

        private ViewHolder(RelativeLayout rootView, TextView id, TextView nama,TextView alamat,TextView hp) {
            this.rootView = rootView;

            this.id = id;
            this.nama = nama;
            this.alamat = alamat;
            this.hp = hp;
        }
        public static LaporanPelangganAdapter.ViewHolder create(RelativeLayout rootView) {
            TextView id  = (TextView) rootView.findViewById(R.id.id);
            TextView nama = (TextView) rootView.findViewById(R.id.nama);
            TextView alamat = (TextView) rootView.findViewById(R.id.alamat);
            TextView hp = (TextView) rootView.findViewById(R.id.hp);
            return new LaporanPelangganAdapter.ViewHolder(rootView,id,nama,alamat,hp);
        }
    }
}
