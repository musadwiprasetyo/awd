package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.Karyawan;
import com.example.awt.model.Pelanggan;

import java.util.ArrayList;
import java.util.List;

public class PelangganAdapter extends ArrayAdapter<Pelanggan> {

    List<Pelanggan> pelangganList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<Pelanggan> arraylist;
    private PelangganAdapter.ClickFunction listener;
    public interface ClickFunction {
        void klikubah(String id,String nama, String nohp, String alamat);
        void klikhapus(String id);
    }
    // Constructors
    public PelangganAdapter(Context context, List<Pelanggan> objects, PelangganAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        pelangganList = objects;
        this.arraylist = new ArrayList<Pelanggan>();
        this.arraylist.addAll(pelangganList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return pelangganList.size();
    }

    @Override
    public Pelanggan getItem(int position) {
        return pelangganList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.pelanggan_list_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final Pelanggan item = getItem(position);
        vh.ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikubah(String.valueOf(item.getId_pelanggan()),String.valueOf(item.getNama_pelanggan()),String.valueOf(item.getNo_hp()),String.valueOf(item.getAlamat()));
            }
        });
        vh.hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikhapus(String.valueOf(item.getId_pelanggan()));
            }
        });
        vh.nama.setText(item.getNama_pelanggan());
        vh.alamat.setText(item.getAlamat());
        vh.nohp.setText(item.getNo_hp());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView nama,nohp,alamat,ubah,hapus;


        private ViewHolder(RelativeLayout rootView, TextView nama,TextView nohp, TextView alamat,TextView ubah,TextView hapus) {
            this.rootView = rootView;
            this.nama = nama;
            this.nohp = nohp;
            this.alamat = alamat;
            this.ubah = ubah;
            this.hapus = hapus;
        }
        public static ViewHolder create(RelativeLayout rootView) {

            TextView nama = (TextView) rootView.findViewById(R.id.nama);
            TextView nohp = (TextView) rootView.findViewById(R.id.nohp);
            TextView alamat  = (TextView) rootView.findViewById(R.id.alamat);
            TextView ubah = (TextView) rootView.findViewById(R.id.ubah);
            TextView hapus = (TextView) rootView.findViewById(R.id.hapus);
            return new ViewHolder (rootView,nama,nohp,alamat,ubah,hapus);
        }
    }
}
