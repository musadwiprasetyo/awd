package com.example.awt.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.Barang;
import com.example.awt.model.Karyawan;

import java.util.ArrayList;
import java.util.List;

public class BarangAdapter extends ArrayAdapter<Barang> {

    List<Barang> barangList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<Barang> arraylist;
    private BarangAdapter.ClickFunction listener;
    public interface ClickFunction {
        void klikubah(String id,String kategori_id, String kode_kategori, String nama_kategori, String kode_barang,
                      String nama_barang, String harga_beli, String harga_barang, String stok,
                      String stok_minimal, String keterangan, String lokasi_rak);
        void klikdetail(String id,String kategori_id, String kode_kategori, String nama_kategori, String kode_barang,
                      String nama_barang, String harga_beli, String harga_barang, String stok,
                      String stok_minimal, String keterangan, String lokasi_rak);
        void klikhapus(String id);
    }
    // Constructors
    public BarangAdapter(Context context, List<Barang> objects, BarangAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        barangList = objects;
        this.arraylist = new ArrayList<Barang>();
        this.arraylist.addAll(barangList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return barangList.size();
    }

    @Override
    public Barang getItem(int position) {
        return barangList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.barang_list_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final Barang item = getItem(position);
        if (Integer.parseInt(item.getStok()) > Integer.parseInt(item.getStok_minimal())){
            vh.bg.setBackgroundColor(Color.GRAY);
        }else{
            vh.bg.setBackgroundColor(Color.RED);
        }
        vh.ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikubah(String.valueOf(item.getId_barang()),
                        String.valueOf(item.getKategori_id()),
                        String.valueOf(item.getKode_kategori()),
                        String.valueOf(item.getNama_kategori()),
                        String.valueOf(item.getKode_barang()),
                        String.valueOf(item.getNama_barang()),
                        String.valueOf(item.getHarga_beli()),
                        String.valueOf(item.getHarga_barang()),
                        String.valueOf(item.getStok()),
                        String.valueOf(item.getStok_minimal()),
                        String.valueOf(item.getKeterangan()),
                        String.valueOf(item.getLokasi_rak())
                );
            }
        });
        vh.kode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikdetail(String.valueOf(item.getId_barang()),
                        String.valueOf(item.getKategori_id()),
                        String.valueOf(item.getKode_kategori()),
                        String.valueOf(item.getNama_kategori()),
                        String.valueOf(item.getKode_barang()),
                        String.valueOf(item.getNama_barang()),
                        String.valueOf(item.getHarga_beli()),
                        String.valueOf(item.getHarga_barang()),
                        String.valueOf(item.getStok()),
                        String.valueOf(item.getStok_minimal()),
                        String.valueOf(item.getKeterangan()),
                        String.valueOf(item.getLokasi_rak())
                );
            }
        });
        vh.hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikhapus(String.valueOf(item.getId_barang()));
            }
        });
        vh.kode.setText(item.getKode_barang());
        vh.nama.setText(item.getNama_barang());
        vh.harga.setText(item.getHarga_barang());
        vh.stok.setText(item.getStok());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView,bg;
        public final TextView kode,nama,harga,stok,ubah,hapus;

        private ViewHolder(RelativeLayout rootView,RelativeLayout bg, TextView kode, TextView nama,TextView harga,TextView stok, TextView ubah,TextView hapus) {
            this.rootView = rootView;
            this.bg = bg;
            this.kode = kode;
            this.nama = nama;
            this.harga = harga;
            this.stok = stok;
            this.ubah = ubah;
            this.hapus = hapus;
        }
        public static ViewHolder create(RelativeLayout rootView) {
            RelativeLayout bg  = (RelativeLayout) rootView.findViewById(R.id.bg);
            TextView kode  = (TextView) rootView.findViewById(R.id.kode);
            TextView nama = (TextView) rootView.findViewById(R.id.nama);
            TextView harga = (TextView) rootView.findViewById(R.id.harga);
            TextView stok = (TextView) rootView.findViewById(R.id.stok);
            TextView ubah = (TextView) rootView.findViewById(R.id.ubah);
            TextView hapus = (TextView) rootView.findViewById(R.id.hapus);
            return new ViewHolder (rootView,bg,kode,nama,harga,stok,ubah,hapus);
        }
    }
}
