package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.Barang;
import com.example.awt.model.BarangKeluar;
import com.example.awt.model.BarangMasuk;
import com.example.awt.model.Karyawan;

import java.util.ArrayList;
import java.util.List;

public class BarangKeluarAdapter extends ArrayAdapter<BarangKeluar> {

    List<BarangKeluar> barangKeluarList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<BarangKeluar> arraylist;
    private BarangKeluarAdapter.ClickFunction listener;
    public interface ClickFunction {

    }
    // Constructors
    public BarangKeluarAdapter(Context context, List<BarangKeluar> objects, BarangKeluarAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        barangKeluarList = objects;
        this.arraylist = new ArrayList<BarangKeluar>();
        this.arraylist.addAll(barangKeluarList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return barangKeluarList.size();
    }

    @Override
    public BarangKeluar getItem(int position) {
        return barangKeluarList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.barang_keluar_list_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final BarangKeluar item = getItem(position);

        vh.tgl.setText(item.getTgl());
        vh.qty.setText("QTY: "+item.getQty());
        vh.kode.setText(item.getKode());
        vh.nama.setText(item.getNama());
        vh.penginput.setText(item.getUsername());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView tgl,qty,kode,nama,penginput;

        private ViewHolder(RelativeLayout rootView, TextView tgl, TextView qty,TextView kode,TextView nama, TextView penginput) {
            this.rootView = rootView;
            this.tgl = tgl;
            this.qty = qty;
            this.kode = kode;
            this.nama = nama;
            this.penginput = penginput;
        }
        public static ViewHolder create(RelativeLayout rootView) {

            TextView tgl  = (TextView) rootView.findViewById(R.id.tgl);
            TextView qty = (TextView) rootView.findViewById(R.id.qty);
            TextView kode = (TextView) rootView.findViewById(R.id.kode);
            TextView nama = (TextView) rootView.findViewById(R.id.nama);
            TextView penginput = (TextView) rootView.findViewById(R.id.penginput);

            return new ViewHolder (rootView,tgl,qty,kode,nama,penginput);
        }
    }
}
