package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.Penjualan;

import java.util.ArrayList;
import java.util.List;

public class PenjualanAdapter extends ArrayAdapter<Penjualan> {

    List<Penjualan> penjualanList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<Penjualan> arraylist;
    private PenjualanAdapter.ClickFunction listener;
    public interface ClickFunction {
        void klikdetail(String id,String kode, String nama, String total, String pembuat, String tgl);
        void klikhapus(String id);
    }
    // Constructors
    public PenjualanAdapter(Context context, List<Penjualan> objects, PenjualanAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        penjualanList = objects;
        this.arraylist = new ArrayList<Penjualan>();
        this.arraylist.addAll(penjualanList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return penjualanList.size();
    }

    @Override
    public Penjualan getItem(int position) {
        return penjualanList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.penjualan_list_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final Penjualan item = getItem(position);
        vh.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikdetail(String.valueOf(item.getId_penjualan()),
                        String.valueOf(item.getKode_penjualan()),String.valueOf(item.getNama_pelanggan()),
                        String.valueOf(item.getTotal()),
                        String.valueOf(item.getPembuat()),
                        String.valueOf(item.getTgl()));
            }
        });

        vh.kode.setText(item.getKode_penjualan());
        vh.pelanggan.setText(item.getNama_pelanggan());
        vh.tgl.setText(item.getTgl());
        vh.total.setText(item.getTotal());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView tgl,kode,total,pelanggan,detail;


        private ViewHolder(RelativeLayout rootView, TextView tgl, TextView kode,TextView total, TextView pelanggan,TextView detail) {
            this.rootView = rootView;
            this.tgl = tgl;
            this.kode = kode;
            this.total = total;
            this.pelanggan = pelanggan;
            this.detail = detail;
        }
        public static ViewHolder create(RelativeLayout rootView) {

            TextView tgl  = (TextView) rootView.findViewById(R.id.tgl);
            TextView kode = (TextView) rootView.findViewById(R.id.kode);
            TextView total = (TextView) rootView.findViewById(R.id.total);
            TextView pelanggan = (TextView) rootView.findViewById(R.id.pelanggan);
            TextView detail = (TextView) rootView.findViewById(R.id.detail);
            return new ViewHolder (rootView,tgl,kode,total,pelanggan,detail);
        }
    }
}
