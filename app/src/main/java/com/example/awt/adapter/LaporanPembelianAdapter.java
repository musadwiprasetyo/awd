package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.LapPembelian;

import java.util.ArrayList;
import java.util.List;

public class LaporanPembelianAdapter extends ArrayAdapter<LapPembelian> {

    List<LapPembelian> pembelianList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<LapPembelian> arraylist;
    private LaporanPembelianAdapter.ClickFunction listener;
    public interface ClickFunction {

    }
    // Constructors
    public LaporanPembelianAdapter(Context context, List<LapPembelian> objects, LaporanPembelianAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        pembelianList = objects;
        this.arraylist = new ArrayList<LapPembelian>();
        this.arraylist.addAll(pembelianList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return pembelianList.size();
    }

    @Override
    public LapPembelian getItem(int position) {
        return pembelianList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LaporanPembelianAdapter.ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.laporan_pembelian_list_view, parent, false);
            vh = LaporanPembelianAdapter.ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (LaporanPembelianAdapter.ViewHolder) convertView.getTag();
        }
        final LapPembelian item = getItem(position);

        vh.id.setText(String.valueOf(position+1));
        vh.nota.setText(String.valueOf(item.getKode_pemesanan()));
        vh.tgl.setText(item.getTgl());
        vh.supplier.setText(item.getNama_supplier());
        vh.oleh.setText("Oleh : "+item.getPembuat());

        vh.barang.setText(item.getKodenama());
        vh.qty.setText(item.getQtysubtotal());
        vh.total.setText(item.getTotal());

        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView id,nota,tgl,supplier,oleh,barang,qty,total;

        private ViewHolder(RelativeLayout rootView, TextView id, TextView nota,TextView tgl,TextView supplier,TextView oleh,TextView barang,TextView qty,TextView total) {
            this.rootView = rootView;

            this.id = id;
            this.nota = nota;
            this.tgl = tgl;
            this.supplier = supplier;
            this.oleh = oleh;
            this.barang = barang;
            this.qty = qty;
            this.total = total;
        }
        public static LaporanPembelianAdapter.ViewHolder create(RelativeLayout rootView) {
            TextView id  = (TextView) rootView.findViewById(R.id.id);
            TextView nota = (TextView) rootView.findViewById(R.id.nota);
            TextView tgl = (TextView) rootView.findViewById(R.id.tgl);
            TextView supplier = (TextView) rootView.findViewById(R.id.supplier);
            TextView oleh = (TextView) rootView.findViewById(R.id.oleh);

            TextView barang = (TextView) rootView.findViewById(R.id.barang);
            TextView qty = (TextView) rootView.findViewById(R.id.qty);
            TextView total = (TextView) rootView.findViewById(R.id.total);
            return new LaporanPembelianAdapter.ViewHolder(rootView,id,nota,tgl,supplier,oleh,barang,qty,total);
        }
    }
}
