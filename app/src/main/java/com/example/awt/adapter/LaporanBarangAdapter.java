package com.example.awt.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.LapBarang;

import java.util.ArrayList;
import java.util.List;

public class LaporanBarangAdapter extends ArrayAdapter<LapBarang> {

    List<LapBarang> barangList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<LapBarang> arraylist;
    private LaporanBarangAdapter.ClickFunction listener;
    public interface ClickFunction {

    }
    // Constructors
    public LaporanBarangAdapter(Context context, List<LapBarang> objects, LaporanBarangAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        barangList = objects;
        this.arraylist = new ArrayList<LapBarang>();
        this.arraylist.addAll(barangList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return barangList.size();
    }

    @Override
    public LapBarang getItem(int position) {
        return barangList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LaporanBarangAdapter.ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.laporan_barang_list_view, parent, false);
            vh = LaporanBarangAdapter.ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (LaporanBarangAdapter.ViewHolder) convertView.getTag();
        }
        final LapBarang item = getItem(position);

        vh.kode.setText(item.getKode_barang());
        vh.nama.setText(item.getNama_barang());
        vh.kategori.setText(item.getNama_kategori());
        vh.lokasi.setText(item.getLokasi_rak());
        vh.beli.setText(item.getHarga_beli());
        vh.jual.setText(item.getHarga_jual());
        vh.stok.setText(item.getStok());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView kode,nama,kategori,lokasi,beli,jual,stok;

        private ViewHolder(RelativeLayout rootView, TextView kode, TextView nama,TextView kategori,TextView lokasi, TextView beli,TextView jual,TextView stok) {
            this.rootView = rootView;

            this.kode = kode;
            this.nama = nama;
            this.kategori = kategori;
            this.lokasi = lokasi;
            this.beli = beli;
            this.jual = jual;
            this.stok = stok;
        }
        public static LaporanBarangAdapter.ViewHolder create(RelativeLayout rootView) {
            TextView kode  = (TextView) rootView.findViewById(R.id.kode);
            TextView nama = (TextView) rootView.findViewById(R.id.nama);
            TextView kategori = (TextView) rootView.findViewById(R.id.kategori);
            TextView lokasi = (TextView) rootView.findViewById(R.id.lokasi);
            TextView beli = (TextView) rootView.findViewById(R.id.beli);
            TextView jual = (TextView) rootView.findViewById(R.id.jual);
            TextView stok = (TextView) rootView.findViewById(R.id.stok);
            return new LaporanBarangAdapter.ViewHolder(rootView,kode,nama,kategori,lokasi,beli,jual,stok);
        }
    }
}
