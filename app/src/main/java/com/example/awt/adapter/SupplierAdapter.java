package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.Supplier;

import java.util.ArrayList;
import java.util.List;

public class SupplierAdapter extends ArrayAdapter<Supplier> {

    List<Supplier> supplierList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<Supplier> arraylist;
    private SupplierAdapter.ClickFunction listener;
    public interface ClickFunction {
        void klikubah(String id,String nama, String nohp, String alamat);
        void klikhapus(String id);
    }
    // Constructors
    public SupplierAdapter(Context context, List<Supplier> objects, SupplierAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        supplierList = objects;
        this.arraylist = new ArrayList<Supplier>();
        this.arraylist.addAll(supplierList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return supplierList.size();
    }

    @Override
    public Supplier getItem(int position) {
        return supplierList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.supplier_list_view, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final Supplier item = getItem(position);
        vh.ubah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikubah(String.valueOf(item.getId_supplier()),String.valueOf(item.getNama()),String.valueOf(item.getNo_hp()),String.valueOf(item.getAlamat()));
            }
        });
        vh.hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikhapus(String.valueOf(item.getId_supplier()));
            }
        });
        vh.alamat.setText(item.getAlamat());
        vh.nama.setText(item.getNama());
        vh.nohp.setText(item.getNo_hp());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView alamat,nama,nohp,ubah,hapus;


        private ViewHolder(RelativeLayout rootView, TextView alamat, TextView nama,TextView nohp, TextView ubah,TextView hapus) {
            this.rootView = rootView;
            this.alamat = alamat;
            this.nama = nama;
            this.nohp = nohp;
            this.ubah = ubah;
            this.hapus = hapus;
        }
        public static ViewHolder create(RelativeLayout rootView) {

            TextView alamat  = (TextView) rootView.findViewById(R.id.alamat);
            TextView nama = (TextView) rootView.findViewById(R.id.nama);
            TextView nohp = (TextView) rootView.findViewById(R.id.nohp);
            TextView ubah = (TextView) rootView.findViewById(R.id.ubah);
            TextView hapus = (TextView) rootView.findViewById(R.id.hapus);
            return new ViewHolder (rootView,alamat,nama,nohp,ubah,hapus);
        }
    }
}
