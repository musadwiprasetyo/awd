package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;
import com.example.awt.model.Karyawan;
import com.example.awt.model.Pelanggan;
import com.example.awt.model.Pemesanan;
import com.example.awt.model.PemesananDetail;
import com.example.awt.model.PenjualanDetail;

import java.util.ArrayList;
import java.util.List;

public class PenjualanDetailListAdapter extends ArrayAdapter<PenjualanDetail> {

    List<PenjualanDetail> penjualanDetailList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<PenjualanDetail> arraylist;
    private PenjualanDetailListAdapter.ClickFunction listener;
    public interface ClickFunction {
        void klikhapus(String id);
    }
    // Constructors
    public PenjualanDetailListAdapter(Context context, List<PenjualanDetail> objects, PenjualanDetailListAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        penjualanDetailList = objects;
        this.arraylist = new ArrayList<PenjualanDetail>();
        this.arraylist.addAll(penjualanDetailList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return penjualanDetailList.size();
    }

    @Override
    public PenjualanDetail getItem(int position) {
        return penjualanDetailList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.penjualan_detail_list_view_detail, parent, false);
            vh = ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        final PenjualanDetail item = getItem(position);

        vh.hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.klikhapus(String.valueOf(item.getKode()));
            }
        });
        vh.kode.setText(item.getKode());
        vh.nama.setText(item.getNama());
        vh.qty.setText(item.getQty());
        vh.subtotal.setText(item.getSub_total());
        vh.total.setText(item.getTotal());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView kode,nama,qty,subtotal,total,hapus;


        private ViewHolder(RelativeLayout rootView, TextView kode, TextView nama,TextView qty, TextView subtotal,TextView total,TextView hapus) {
            this.rootView = rootView;
            this.kode = kode;
            this.nama = nama;
            this.qty = qty;
            this.subtotal = subtotal;
            this.total = total;
            this.hapus = hapus;
        }
        public static ViewHolder create(RelativeLayout rootView) {

            TextView kode  = (TextView) rootView.findViewById(R.id.kode);
            TextView nama = (TextView) rootView.findViewById(R.id.nama);
            TextView qty = (TextView) rootView.findViewById(R.id.qty);
            TextView subtotal = (TextView) rootView.findViewById(R.id.subtotal);
            TextView total = (TextView) rootView.findViewById(R.id.total);
            TextView hapus = (TextView) rootView.findViewById(R.id.hapus);
            return new ViewHolder (rootView,kode,nama,qty,subtotal,total,hapus);
        }
    }
}
