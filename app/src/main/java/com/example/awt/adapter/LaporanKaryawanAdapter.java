package com.example.awt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.awt.R;

import com.example.awt.model.LapKaryawan;

import java.util.ArrayList;
import java.util.List;

public class LaporanKaryawanAdapter extends ArrayAdapter<LapKaryawan> {

    List<LapKaryawan> karyawanList;
    Context context;
    private LayoutInflater mInflater;
    private ArrayList<LapKaryawan> arraylist;
    private LaporanKaryawanAdapter.ClickFunction listener;
    public interface ClickFunction {

    }
    // Constructors
    public LaporanKaryawanAdapter(Context context, List<LapKaryawan> objects, LaporanKaryawanAdapter.ClickFunction listener) {
        super(context, 0, objects);
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        karyawanList = objects;
        this.arraylist = new ArrayList<LapKaryawan>();
        this.arraylist.addAll(karyawanList);
        this.listener  = listener;
    }
    @Override
    public int getCount() {
        return karyawanList.size();
    }

    @Override
    public LapKaryawan getItem(int position) {
        return karyawanList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LaporanKaryawanAdapter.ViewHolder vh;
        if (convertView == null) {
            View view = mInflater.inflate(R.layout.laporan_karyawan_list_view, parent, false);
            vh = LaporanKaryawanAdapter.ViewHolder.create((RelativeLayout) view);
            view.setTag(vh);
        } else {
            vh = (LaporanKaryawanAdapter.ViewHolder) convertView.getTag();
        }
        final LapKaryawan item = getItem(position);

        vh.idkary.setText(String.valueOf(item.getId_karyawan()));
        vh.nama.setText(item.getNama());
        vh.akses.setText(item.getNama_akses());
        return vh.rootView;
    }

    private static class ViewHolder {
        public final RelativeLayout rootView;
        public final TextView idkary,nama,akses;


        private ViewHolder(RelativeLayout rootView, TextView idkary, TextView nama,TextView akses) {
            this.rootView = rootView;
            this.idkary = idkary;
            this.nama = nama;
            this.akses = akses;
        }
        public static LaporanKaryawanAdapter.ViewHolder create(RelativeLayout rootView) {

            TextView idkary  = (TextView) rootView.findViewById(R.id.idkary);
            TextView nama = (TextView) rootView.findViewById(R.id.nama);
            TextView akses = (TextView) rootView.findViewById(R.id.akses);
            return new LaporanKaryawanAdapter.ViewHolder(rootView,idkary,nama,akses);
        }
    }
}
