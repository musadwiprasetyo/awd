package com.example.awt;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.adapter.PemesananDetailAdapter;
import com.example.awt.adapter.PemesananDetailListAdapter;
import com.example.awt.adapter.SupplierAdapter;
import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.model.PemesananDetail;
import com.example.awt.model.PilihBarang;
import com.example.awt.model.PilihSupplier;
import com.example.awt.model.Supplier;
import com.example.awt.utils.DatabaseHandler;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;

public class TambahPemesananActivity extends AppCompatActivity implements PemesananDetailListAdapter.ClickFunction {
    ImageView back, tambah;
    Button simpan,hapus;
    Spinner supplier;
    EditText tgl;
    TextView penginput,total;
    TextView kodeorder;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    Intent intent;
    ProgressBar progressBar;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    PemesananDetailListAdapter pemesananDetailAdapter;
    private List<PemesananDetail> pemesananDetailList;
    ListView listView;
    DatabaseHandler dbcenter;
    SQLiteDatabase db;
    Cursor cursor,cursorx,cx5;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemesanan_tambah);
        tgl =  (EditText) findViewById(R.id.tgl);
        penginput =  (TextView) findViewById(R.id.penginput);
        total =  (TextView) findViewById(R.id.total);
        kodeorder =  (TextView) findViewById(R.id.kodeorder);
        supplier =  (Spinner) findViewById(R.id.supplier);
        simpan =  (Button) findViewById(R.id.simpan);
        hapus =  (Button) findViewById(R.id.hapus);
        back =  (ImageView) findViewById(R.id.back);
        tambah =  (ImageView) findViewById(R.id.tambah);
        listView      = (ListView) findViewById(R.id.listView);
        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        dbcenter = new DatabaseHandler(this);
        db= dbcenter.getReadableDatabase();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpanData();
            }
        });
        Random random = new Random();
        int x = random.nextInt(900) + 100;
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = sdf.format(c.getTime());

        SimpleDateFormat sdfx = new SimpleDateFormat("yyyyddMM");
        String strDatex = sdfx.format(c.getTime());

        kodeorder.setText(strDatex+sharedPrefManager.getID()+x);
        tgl.setText(strDate);
        tgl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });
        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TambahBarangPemesananActivity.class);
                startActivity(intent);
            }
        });
        hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HapusSemua();
            }
        });
        dataSupplier();
        dataBarang();
        Total();
        penginput.setText(sharedPrefManager.getUsername());
    }
    private void Total() {
        cx5 = db.rawQuery("select  sum(total) FROM table_pemesanan_detail;",null);
        if(cx5.moveToFirst())
            total.setText(String.valueOf(cx5.getInt(0)));
        else
            total.setText("0");
        cx5.close();
    }
    @Override
    protected void onResume() {
        dataBarang();
        Total();
        super.onResume();
    }

    private void dataBarang() {
        cursor = db.rawQuery("SELECT * FROM table_pemesanan_detail",null);
        cursor.moveToFirst();
        ArrayList<PemesananDetail> pemesananArrayList = new ArrayList<>();
        for (int i = 0; i < cursor.getCount(); i++) {
            pemesananArrayList.add(new PemesananDetail(Integer.parseInt(cursor.getString(1)),
                    cursor.getString(2).toString(),
                    cursor.getString(3).toString(),
                    cursor.getString(4).toString(),
                    cursor.getString(5).toString(),
                    cursor.getString(6).toString()));
            cursor.moveToNext();
        }
        pemesananDetailAdapter = new PemesananDetailListAdapter(TambahPemesananActivity.this, pemesananArrayList,TambahPemesananActivity.this);
        listView.setAdapter(pemesananDetailAdapter);
    }
    private void dataSupplier() {
        mApiService.getSupplier()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("TAG HASIL",response.code()+"");
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("code").equals("200")) {
                                    String message = jsonRESULTS.getString("message");
                                    JSONArray data = jsonRESULTS.getJSONArray("data");
                                    ArrayList<Supplier> supplierArrayList = new ArrayList<>();
                                    ArrayList<PilihSupplier> pilihSuppliers = new ArrayList<>();
                                    pilihSuppliers.add(new PilihSupplier("0","Pilih Supplier"));
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject datarray = data.getJSONObject(i);
                                        pilihSuppliers.add(new PilihSupplier(datarray.getString("id_supplier"),datarray.getString("nama")));
                                    }
                                    ArrayAdapter<PilihSupplier> adapter = new ArrayAdapter<PilihSupplier>(TambahPemesananActivity.this, android.R.layout.simple_spinner_dropdown_item,pilihSuppliers);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    supplier.setAdapter(adapter);
                                }else{
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(TambahPemesananActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                String message = jsonRESULTS.getString("message");
                                Toast.makeText(TambahPemesananActivity.this, message, Toast.LENGTH_SHORT).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.i("TAG","ini terjadi error");
                        Toast.makeText(TambahPemesananActivity.this, "Error. Try Again", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void showDateDialog(){
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(TambahPemesananActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tgl.setText(dateFormatter.format(newDate.getTime()));
            }
        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
    private void SimpanData() {
        PilihSupplier pilihSupplier = (PilihSupplier) supplier.getSelectedItem();
        if (pilihSupplier.getId().equals("0")){
            Toast.makeText(TambahPemesananActivity.this, "Silahkan Pilih Supplier terlebih dahulu", Toast.LENGTH_SHORT).show();
        }else if(tgl.getText().toString().equals(""))
        {
            Toast.makeText(TambahPemesananActivity.this,"Tgl tidak boleh kosong",Toast.LENGTH_SHORT).show();
        }else if(total.getText().toString().equals("") | total.getText().toString().equals("0")){
            Toast.makeText(TambahPemesananActivity.this,"Total tidak boleh kosong / 0", Toast.LENGTH_SHORT).show();
        }else{
            mApiService.kirimPemesanan(kodeorder.getText().toString(), pilihSupplier.getId().toString(),sharedPrefManager.getID(),
                    total.getText().toString(),sharedPrefManager.getUsername(),tgl.getText().toString())
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            if (response.isSuccessful()){
                                try {
                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                    if (jsonRESULTS.getString("code").equals("200")){
                                        String message = jsonRESULTS.getString("message");
                                        cursor = db.rawQuery("SELECT * FROM table_pemesanan_detail",null);
                                        cursor.moveToFirst();
                                        for (int i = 1; i <=  cursor.getCount(); i++) {
                                            mApiService.kirimPemesananDetail(kodeorder.getText().toString(),cursor.getString(1).toString(),
                                                    cursor.getString(4).toString(),
                                                    cursor.getString(5).toString(),
                                                    cursor.getString(6).toString())
                                                    .enqueue(new Callback<ResponseBody>() {
                                                        @Override
                                                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                                            if (response.isSuccessful()){
                                                                try {
                                                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                                                    if (jsonRESULTS.getString("code").equals("200")){
                                                                        String message = jsonRESULTS.getString("message");

                                                                    } else {
                                                                        String message = jsonRESULTS.getString("message");
                                                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                                                        progressBar.setVisibility(View.INVISIBLE);
                                                                    }
                                                                } catch (JSONException e) {
                                                                    e.printStackTrace();
                                                                } catch (IOException e) {
                                                                    e.printStackTrace();
                                                                }
                                                            } else {

                                                            }
                                                        }
                                                        @Override
                                                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                            Toast.makeText(getApplicationContext(), "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });

                                            cursor.moveToNext();
                                            if (i==cursor.getCount()){
                                                db.execSQL("DELETE FROM table_pemesanan_detail");
                                                dataBarang();
                                                Total();
                                                Toast.makeText(getApplicationContext(), "Pemesanan berhasil dikirim", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    } else {
                                        String message = jsonRESULTS.getString("message");
                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                    String message = jsonRESULTS.getString("message");
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Periksa Koneksi Internet Anda", Toast.LENGTH_SHORT).show();
//                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });
        }
    }
    public void setSuccessPengajuan(String message) {
        new AlertDialog.Builder(TambahPemesananActivity.this).setTitle("YES").setMessage(message).setCancelable(false).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                db.execSQL("DELETE FROM table_pemesanan_detail");
                dataBarang();
                Total();
            }
        }).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void klikhapus(String id) {
        db.execSQL("DELETE FROM table_pemesanan_detail where kode_barang = '"+id+"'");
        dataBarang();
        Total();
    }
    public void HapusSemua() {
        db.execSQL("DELETE FROM table_pemesanan_detail;");
        dataBarang();
        Total();
    }
}
