package com.example.awt.model;

public class LaporanWMA {
    private static final String TAG = "LaporanWMA";

    private int id_rekap;
    private String tahun;
    private String bulan;
    private String penjualan;
    private String wma;
    private String error;
    private String mad;

    public LaporanWMA(int id_rekap, String tahun, String bulan, String penjualan, String wma, String error, String mad) {
        this.id_rekap = id_rekap;
        this.tahun = tahun;
        this.bulan = bulan;
        this.penjualan = penjualan;
        this.wma = wma;
        this.error = error;
        this.mad = mad;
    }

    public int getId_rekap() {
        return id_rekap;
    }

    public void setId_rekap(int id_rekap) {
        this.id_rekap = id_rekap;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getPenjualan() {
        return penjualan;
    }

    public void setPenjualan(String penjualan) {
        this.penjualan = penjualan;
    }

    public String getWma() {
        return wma;
    }

    public void setWma(String wma) {
        this.wma = wma;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMad() {
        return mad;
    }

    public void setMad(String mad) {
        this.mad = mad;
    }
}
