package com.example.awt.model;

public class LapKaryawan {
    private static final String TAG = "Laporan Karyawan";

    private int id_karyawan;
    private String nama;
    private String nama_akses;

    public LapKaryawan(int id_karyawan, String nama, String nama_akses) {
        this.id_karyawan = id_karyawan;
        this.nama = nama;
        this.nama_akses = nama_akses;
    }

    public int getId_karyawan() {
        return id_karyawan;
    }

    public void setId_karyawan(int id_karyawan) {
        this.id_karyawan = id_karyawan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama_akses() {
        return nama_akses;
    }

    public void setNama_akses(String nama_akses) {
        this.nama_akses = nama_akses;
    }
}
