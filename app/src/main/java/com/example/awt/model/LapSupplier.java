package com.example.awt.model;

public class LapSupplier {
    private static final String TAG = "Lap Supplier";

    private int id_supplier;
    private String nama;
    private String no_hp;
    private String alamat;

    public LapSupplier(int id_supplier, String nama, String no_hp, String alamat) {
        this.id_supplier = id_supplier;
        this.nama = nama;
        this.no_hp = no_hp;
        this.alamat = alamat;
    }

    public int getId_supplier() {
        return id_supplier;
    }

    public void setId_supplier(int id_supplier) {
        this.id_supplier = id_supplier;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
