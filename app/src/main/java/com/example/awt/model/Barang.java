package com.example.awt.model;

public class Barang {
    private static final String TAG = "Barang";

    private int id_barang;
    private String kategori_id;
    private String kode_kategori;
    private String nama_kategori;
    private String kode_barang;
    private String nama_barang;
    private String harga_beli;
    private String harga_barang;
    private String stok;
    private String stok_minimal;
    private String keterangan;
    private String lokasi_rak;


    public Barang(int id_barang, String kategori_id, String kode_kategori, String nama_kategori, String kode_barang, String nama_barang, String harga_beli, String harga_barang, String stok, String stok_minimal, String keterangan, String lokasi_rak) {
        this.id_barang = id_barang;
        this.kategori_id = kategori_id;
        this.kode_kategori = kode_kategori;
        this.nama_kategori = nama_kategori;
        this.kode_barang = kode_barang;
        this.nama_barang = nama_barang;
        this.harga_beli = harga_beli;
        this.harga_barang = harga_barang;
        this.stok = stok;
        this.stok_minimal = stok_minimal;
        this.keterangan = keterangan;
        this.lokasi_rak = lokasi_rak;
    }

    public int getId_barang() {
        return id_barang;
    }

    public void setId_barang(int id_barang) {
        this.id_barang = id_barang;
    }

    public String getKategori_id() {
        return kategori_id;
    }

    public void setKategori_id(String kategori_id) {
        this.kategori_id = kategori_id;
    }

    public String getKode_kategori() {
        return kode_kategori;
    }

    public void setKode_kategori(String kode_kategori) {
        this.kode_kategori = kode_kategori;
    }

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }

    public String getKode_barang() {
        return kode_barang;
    }

    public void setKode_barang(String kode_barang) {
        this.kode_barang = kode_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getHarga_beli() {
        return harga_beli;
    }

    public void setHarga_beli(String harga_beli) {
        this.harga_beli = harga_beli;
    }

    public String getHarga_barang() {
        return harga_barang;
    }

    public void setHarga_barang(String harga_barang) {
        this.harga_barang = harga_barang;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getStok_minimal() {
        return stok_minimal;
    }

    public void setStok_minimal(String stok_minimal) {
        this.stok_minimal = stok_minimal;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getLokasi_rak() {
        return lokasi_rak;
    }

    public void setLokasi_rak(String lokasi_rak) {
        this.lokasi_rak = lokasi_rak;
    }
}
