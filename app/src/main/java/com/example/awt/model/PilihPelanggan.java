package com.example.awt.model;

public class PilihPelanggan {
    private static final String TAG = "PilihPelanggan";

    private String id;
    private String name;

    public PilihPelanggan(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    //to display object as a string in spinner
    @Override
    public String toString() {
        return name;
    }
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof PilihPelanggan){
            PilihPelanggan c = (PilihPelanggan )obj;
            if(c.getId()==id ) return true;
        }

        return false;
    }
}
