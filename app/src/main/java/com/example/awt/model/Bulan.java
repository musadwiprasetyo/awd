package com.example.awt.model;

public class Bulan {
    private static final String TAG = "Bulan";

    private String kode;
    private String name;

    public Bulan(String kode, String name) {

        this.kode = kode;
        this.name = name;
    }


    public String getKode() {
        return kode;
    }
    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    //to display object as a string in spinner
    @Override
    public String toString() {
        return name;
    }
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Bulan){
            Bulan c = (Bulan )obj;
            if(c.getKode()==kode ) return true;
        }
        return false;
    }
}
