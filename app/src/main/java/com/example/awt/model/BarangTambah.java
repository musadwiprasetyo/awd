package com.example.awt.model;

public class BarangTambah {
    private static final String TAG = "Barang";

    private int id_barang;
    private String kode_barang;
    private String nama_barang;
    private String harga_barang;
    private String stok;
    private String keterangan;
    private String rak;

    public BarangTambah(int id_barang, String kode_barang, String nama_barang, String harga_barang, String stok, String keterangan, String rak) {
        this.id_barang = id_barang;
        this.kode_barang = kode_barang;
        this.nama_barang = nama_barang;
        this.harga_barang = harga_barang;
        this.stok = stok;
        this.keterangan = keterangan;
        this.rak = rak;
    }

    public int getId_barang() {
        return id_barang;
    }

    public void setId_barang(int id_barang) {
        this.id_barang = id_barang;
    }

    public String getKode_barang() {
        return kode_barang;
    }

    public void setKode_barang(String kode_barang) {
        this.kode_barang = kode_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getHarga_barang() {
        return harga_barang;
    }

    public void setHarga_barang(String harga_barang) {
        this.harga_barang = harga_barang;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getRak() {
        return rak;
    }

    public void setRak(String rak) {
        this.rak = rak;
    }
}
