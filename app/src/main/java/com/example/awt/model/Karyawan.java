package com.example.awt.model;

public class Karyawan {
    private static final String TAG = "Karyawan";

    private int id_karyawan;
    private String nama;
    private String no_hp;
    private String username;
    private String akses_id;
    private String nama_akses;

    public Karyawan(int id_karyawan, String nama, String no_hp, String username, String akses_id, String nama_akses) {
        this.id_karyawan = id_karyawan;
        this.nama = nama;
        this.no_hp = no_hp;
        this.username = username;
        this.akses_id = akses_id;
        this.nama_akses = nama_akses;
    }

    public int getId_karyawan() {
        return id_karyawan;
    }

    public void setId_karyawan(int id_karyawan) {
        this.id_karyawan = id_karyawan;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAkses_id() {
        return akses_id;
    }

    public void setAkses_id(String akses_id) {
        this.akses_id = akses_id;
    }

    public String getNama_akses() {
        return nama_akses;
    }

    public void setNama_akses(String nama_akses) {
        this.nama_akses = nama_akses;
    }
}
