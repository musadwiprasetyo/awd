package com.example.awt.model;

public class Pelanggan {
    private static final String TAG = "Pelanggan";

    private int id_pelanggan;
    private String nama_pelanggan;
    private String no_hp;
    private String alamat;

    public Pelanggan(int id_pelanggan, String nama_pelanggan, String no_hp, String alamat) {
        this.id_pelanggan = id_pelanggan;
        this.nama_pelanggan = nama_pelanggan;
        this.no_hp = no_hp;
        this.alamat = alamat;
    }

    public int getId_pelanggan() {
        return id_pelanggan;
    }

    public void setId_pelanggan(int id_pelanggan) {
        this.id_pelanggan = id_pelanggan;
    }

    public String getNama_pelanggan() {
        return nama_pelanggan;
    }

    public void setNama_pelanggan(String nama_pelanggan) {
        this.nama_pelanggan = nama_pelanggan;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
