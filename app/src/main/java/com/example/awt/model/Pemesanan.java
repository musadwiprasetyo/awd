package com.example.awt.model;

public class Pemesanan {
    private static final String TAG = "Pemesanan";

    private int id_pemesanan;
    private String kode_pemesanan;
    private String nama_supplier;
    private String total;
    private String pembuat;
    private String tgl;

    public Pemesanan(int id_pemesanan, String kode_pemesanan, String nama_supplier, String total, String pembuat, String tgl) {
        this.id_pemesanan = id_pemesanan;
        this.kode_pemesanan = kode_pemesanan;
        this.nama_supplier = nama_supplier;
        this.total = total;
        this.pembuat = pembuat;
        this.tgl = tgl;
    }

    public int getId_pemesanan() {
        return id_pemesanan;
    }

    public void setId_pemesanan(int id_pemesanan) {
        this.id_pemesanan = id_pemesanan;
    }

    public String getKode_pemesanan() {
        return kode_pemesanan;
    }

    public void setKode_pemesanan(String kode_pemesanan) {
        this.kode_pemesanan = kode_pemesanan;
    }

    public String getNama_supplier() {
        return nama_supplier;
    }

    public void setNama_supplier(String nama_supplier) {
        this.nama_supplier = nama_supplier;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPembuat() {
        return pembuat;
    }

    public void setPembuat(String pembuat) {
        this.pembuat = pembuat;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }
}
