package com.example.awt.model;

public class PenjualanDetail {
    private static final String TAG = "Penjualan Detail";

    private int id_penjualandetail;
    private String kode;
    private String nama;
    private String qty;
    private String sub_total;
    private String total;

    public PenjualanDetail(int id_penjualandetail, String kode, String nama, String qty, String sub_total, String total) {
        this.id_penjualandetail = id_penjualandetail;
        this.kode = kode;
        this.nama = nama;
        this.qty = qty;
        this.sub_total = sub_total;
        this.total = total;
    }

    public int getId_penjualandetail() {
        return id_penjualandetail;
    }

    public void setId_penjualandetail(int id_penjualandetail) {
        this.id_penjualandetail = id_penjualandetail;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getSub_total() {
        return sub_total;
    }

    public void setSub_total(String sub_total) {
        this.sub_total = sub_total;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
