package com.example.awt.model;

public class Penjualan {
    private static final String TAG = "Penjualan";

    private int id_penjualan;
    private String kode_penjualan;
    private String nama_pelanggan;
    private String total;
    private String pembuat;
    private String tgl;

    public Penjualan(int id_penjualan, String kode_penjualan, String nama_pelanggan, String total, String pembuat, String tgl) {
        this.id_penjualan = id_penjualan;
        this.kode_penjualan = kode_penjualan;
        this.nama_pelanggan = nama_pelanggan;
        this.total = total;
        this.pembuat = pembuat;
        this.tgl = tgl;
    }

    public int getId_penjualan() {
        return id_penjualan;
    }

    public void setId_penjualan(int id_penjualan) {
        this.id_penjualan = id_penjualan;
    }

    public String getKode_penjualan() {
        return kode_penjualan;
    }

    public void setKode_penjualan(String kode_penjualan) {
        this.kode_penjualan = kode_penjualan;
    }

    public String getNama_pelanggan() {
        return nama_pelanggan;
    }

    public void setNama_pelanggan(String nama_pelanggan) {
        this.nama_pelanggan = nama_pelanggan;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPembuat() {
        return pembuat;
    }

    public void setPembuat(String pembuat) {
        this.pembuat = pembuat;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }
}
