package com.example.awt.model;

public class LapPembelian {
    private static final String TAG = "Laporan Pembelian";

    private int id_pemesanandetail;
    private String kode_pemesanan;
    private String tgl;
    private String nama_supplier;
    private String kodenama;
    private String qtysubtotal;
    private String total;
    private String pembuat;

    public LapPembelian(int id_pemesanandetail, String kode_pemesanan, String tgl, String nama_supplier, String kodenama, String qtysubtotal, String total, String pembuat) {
        this.id_pemesanandetail = id_pemesanandetail;
        this.kode_pemesanan = kode_pemesanan;
        this.tgl = tgl;
        this.nama_supplier = nama_supplier;
        this.kodenama = kodenama;
        this.qtysubtotal = qtysubtotal;
        this.total = total;
        this.pembuat = pembuat;
    }

    public int getId_pemesanandetail() {
        return id_pemesanandetail;
    }

    public void setId_pemesanandetail(int id_pemesanandetail) {
        this.id_pemesanandetail = id_pemesanandetail;
    }

    public String getKode_pemesanan() {
        return kode_pemesanan;
    }

    public void setKode_pemesanan(String kode_pemesanan) {
        this.kode_pemesanan = kode_pemesanan;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getNama_supplier() {
        return nama_supplier;
    }

    public void setNama_supplier(String nama_supplier) {
        this.nama_supplier = nama_supplier;
    }

    public String getKodenama() {
        return kodenama;
    }

    public void setKodenama(String kodenama) {
        this.kodenama = kodenama;
    }

    public String getQtysubtotal() {
        return qtysubtotal;
    }

    public void setQtysubtotal(String qtysubtotal) {
        this.qtysubtotal = qtysubtotal;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPembuat() {
        return pembuat;
    }

    public void setPembuat(String pembuat) {
        this.pembuat = pembuat;
    }
}
