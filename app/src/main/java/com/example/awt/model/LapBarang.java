package com.example.awt.model;

public class LapBarang {
    private static final String TAG = "Laporan Barang";

    private int id_barang;
    private String kode_barang;
    private String nama_barang;
    private String nama_kategori;
    private String lokasi_rak;
    private String harga_beli;
    private String harga_jual;
    private String stok;

    public LapBarang(int id_barang, String kode_barang, String nama_barang, String nama_kategori, String lokasi_rak, String harga_beli, String harga_jual, String stok) {
        this.id_barang = id_barang;
        this.kode_barang = kode_barang;
        this.nama_barang = nama_barang;
        this.nama_kategori = nama_kategori;
        this.lokasi_rak = lokasi_rak;
        this.harga_beli = harga_beli;
        this.harga_jual = harga_jual;
        this.stok = stok;
    }

    public int getId_barang() {
        return id_barang;
    }

    public void setId_barang(int id_barang) {
        this.id_barang = id_barang;
    }

    public String getKode_barang() {
        return kode_barang;
    }

    public void setKode_barang(String kode_barang) {
        this.kode_barang = kode_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }

    public String getLokasi_rak() {
        return lokasi_rak;
    }

    public void setLokasi_rak(String lokasi_rak) {
        this.lokasi_rak = lokasi_rak;
    }

    public String getHarga_beli() {
        return harga_beli;
    }

    public void setHarga_beli(String harga_beli) {
        this.harga_beli = harga_beli;
    }

    public String getHarga_jual() {
        return harga_jual;
    }

    public void setHarga_jual(String harga_jual) {
        this.harga_jual = harga_jual;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }
}
