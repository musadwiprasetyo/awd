package com.example.awt.model;

public class PilihBarang {
    private static final String TAG = "Pilih Barang";

    private String id;
    private String kode;
    private String name;

    public PilihBarang(String id,String kode, String name) {
        this.id = id;
        this.kode = kode;
        this.name = name;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getKode() {
        return kode;
    }
    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    //to display object as a string in spinner
    @Override
    public String toString() {
        return name;
    }
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof PilihBarang){
            PilihBarang c = (PilihBarang )obj;
            if(c.getId()==id ) return true;
        }
        return false;
    }
}

