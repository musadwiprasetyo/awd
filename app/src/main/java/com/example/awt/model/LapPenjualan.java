package com.example.awt.model;

public class LapPenjualan {
    private static final String TAG = "Laporan Penjualan";

    private int id_pemesanandetail;
    private String kode_penjualan;
    private String tgl;
    private String nama_pelanggan;
    private String kodenama;
    private String qtysubtotal;
    private String total;
    private String pembuat;

    public LapPenjualan(int id_pemesanandetail, String kode_penjualan, String tgl, String nama_pelanggan, String kodenama, String qtysubtotal, String total, String pembuat) {
        this.id_pemesanandetail = id_pemesanandetail;
        this.kode_penjualan = kode_penjualan;
        this.tgl = tgl;
        this.nama_pelanggan = nama_pelanggan;
        this.kodenama = kodenama;
        this.qtysubtotal = qtysubtotal;
        this.total = total;
        this.pembuat = pembuat;
    }

    public int getId_pemesanandetail() {
        return id_pemesanandetail;
    }

    public void setId_pemesanandetail(int id_pemesanandetail) {
        this.id_pemesanandetail = id_pemesanandetail;
    }

    public String getKode_penjualan() {
        return kode_penjualan;
    }

    public void setKode_penjualan(String kode_penjualan) {
        this.kode_penjualan = kode_penjualan;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getNama_pelanggan() {
        return nama_pelanggan;
    }

    public void setNama_pelanggan(String nama_pelanggan) {
        this.nama_pelanggan = nama_pelanggan;
    }

    public String getKodenama() {
        return kodenama;
    }

    public void setKodenama(String kodenama) {
        this.kodenama = kodenama;
    }

    public String getQtysubtotal() {
        return qtysubtotal;
    }

    public void setQtysubtotal(String qtysubtotal) {
        this.qtysubtotal = qtysubtotal;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPembuat() {
        return pembuat;
    }

    public void setPembuat(String pembuat) {
        this.pembuat = pembuat;
    }
}
