package com.example.awt.model;

public class BarangKeluar {
    private static final String TAG = "Barang Keluar";

    private int id_penjualan_detail;
    private String kode;
    private String nama;
    private String qty;
    private String username;
    private String sub_total;
    private String total;
    private String tgl;


    public BarangKeluar(int id_penjualan_detail, String kode, String nama, String qty, String username, String sub_total, String total, String tgl) {
        this.id_penjualan_detail = id_penjualan_detail;
        this.kode = kode;
        this.nama = nama;
        this.qty = qty;
        this.username = username;
        this.sub_total = sub_total;
        this.total = total;
        this.tgl = tgl;
    }

    public int getId_penjualan_detail() {
        return id_penjualan_detail;
    }

    public void setId_penjualan_detail(int id_penjualan_detail) {
        this.id_penjualan_detail = id_penjualan_detail;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSub_total() {
        return sub_total;
    }

    public void setSub_total(String sub_total) {
        this.sub_total = sub_total;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }
}
