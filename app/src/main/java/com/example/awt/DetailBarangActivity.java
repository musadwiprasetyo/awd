package com.example.awt;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.awt.api.ApiService;
import com.example.awt.api.BaseApi;
import com.example.awt.utils.SharedPrefManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;

public class DetailBarangActivity extends AppCompatActivity {
    ImageView back;
    TextView nama,kode,kategori,hargabeli,hargajual,rak,keterangan,stok,stokminimal;
    SharedPrefManager sharedPrefManager;
    ApiService mApiService;
    TextView title;
    Intent intent;
    Intent datadetail;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barang_detail);
        title =  (TextView) findViewById(R.id.title);
        nama =  (TextView) findViewById(R.id.nama);
        kode =  (TextView) findViewById(R.id.kode);
        kategori =  (TextView) findViewById(R.id.kategori);
        hargabeli =  (TextView) findViewById(R.id.hargabeli);
        hargajual =  (TextView) findViewById(R.id.hargajual);
        rak =  (TextView) findViewById(R.id.rak);
        keterangan =  (TextView) findViewById(R.id.keterangan);
        stok =  (TextView) findViewById(R.id.stok);
        stokminimal =  (TextView) findViewById(R.id.stokminimal);
        back =  (ImageView) findViewById(R.id.back);


        sharedPrefManager = new SharedPrefManager(this);
        mApiService = BaseApi.getAPIService();
        datadetail = getIntent();
        title.setText("Detail Data "+datadetail.getStringExtra("nama"));
        nama.setText(datadetail.getStringExtra("nama"));
        kode.setText(datadetail.getStringExtra("kode"));
        kategori.setText(datadetail.getStringExtra("kode_kategori")+'-'+datadetail.getStringExtra("nama_kategori"));
        hargabeli.setText(datadetail.getStringExtra("harga_beli"));
        hargajual.setText(datadetail.getStringExtra("harga"));
        keterangan.setText(datadetail.getStringExtra("keterangan"));
        rak.setText(datadetail.getStringExtra("lokasi_rak"));
        stok.setText(datadetail.getStringExtra("stok"));
        stokminimal.setText(datadetail.getStringExtra("stok_minimal"));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
